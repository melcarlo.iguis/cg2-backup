import React, { useContext, useState, useEffect } from 'react'
import { Navigate, Link, useNavigate } from 'react-router-dom'

import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import Swal from 'sweetalert2'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'

import AppContext from '../../../AppContext'
import api from '../../../api/api'

const Login = () => {
  const navigate = useNavigate()

  const { user, setUser } = useContext(AppContext)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  let userIdval = localStorage.getItem('userId')

  // function to login user
  const loginUser = async (e) => {
    e.preventDefault()
    const input = {
      email: email,
      password: password,
    }

    await api.post('/users/login', input).then((result) => {
      if (typeof result.data.access !== 'undefined') {
        localStorage.setItem('token', result.data.access)
        retrieveUserDetails(result.data.access)
        Swal.fire({
          title: 'Login Successful!',
          icon: 'success',
          text: 'Welcome',
        })
      } else {
        Swal.fire({
          title: 'Authentication failed',
          icon: 'error',
          text: 'Check login details',
        })
      }
    })
  }

  // function for retrieving user's details
  const retrieveUserDetails = async (token) => {
    await api
      .get('/users/details', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((result) => {
        console.log(result.data)

        // store user details on localStorage upon log in
        localStorage.setItem('userId', result.data.id)
        localStorage.setItem('email', result.data.email)
        localStorage.setItem('name', result.data.username)
        localStorage.setItem('userType', result.data.userType)

        setUser({
          id: result.data.id,
          userType: result.data.userType,
        })

        navigate('/')
      })
  }

  return userIdval != null ? (
    <Navigate to="/" />
  ) : (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={6}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={(e) => loginUser(e)}>
                    <h1>Login</h1>
                    <p className="text-medium-emphasis">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        type="password"
                        placeholder="Password"
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton color="primary" className="px-4" type="submit">
                          Login
                        </CButton>
                      </CCol>
                      {/* <CCol xs={6} className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol>*/}
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              {/* <CCard className="text-white bg-primary py-5" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>Don't have an account?</p>
                    <Link to="/register">
                      <CButton color="primary" className="mt-1" active tabIndex={-1}>
                        Register Now!
                      </CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard> */}
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
