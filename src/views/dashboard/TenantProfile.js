import React, { useState, useEffect, useContext, useRef } from 'react'
import { Row, Col, Form, Table } from 'react-bootstrap'
import '../../App.css'
import { Grid, Box, CardActionArea, CardActions } from '@mui/material'
import {
  faBedPulse,
  faBurger,
  faClipboard,
  faFaceGrin,
  faHandDots,
  faHeartPulse,
  faPersonFalling,
  faUserDoctor,
  faBell,
  faTriangleExclamation,
  faCircleInfo,
  faPhone,
  faHospital,
  faHospitalUser,
  faCalendarCheck,
  faCalendarPlus,
  faCrutch,
  faPeopleGroup,
  faFilePdf,
  faCircleCheck,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Button from '@mui/material/Button'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import Card from '@mui/material/Card'
import AddIcon from '@mui/icons-material/Add'
import PendingActionsIcon from '@mui/icons-material/PendingActions'
import AddBoxIcon from '@mui/icons-material/AddBox'
import TextSnippetIcon from '@mui/icons-material/TextSnippet'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
import FolderSharedIcon from '@mui/icons-material/FolderShared'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import { CContainer, CRow, CCol, CCardHeader } from '@coreui/react'
import { styled } from '@mui/system'
import SwitchUnstyled, { switchUnstyledClasses } from '@mui/base/SwitchUnstyled'

import Menu from '@mui/material/Menu'
import HomeIcon from '@mui/icons-material/Home'
import DateRangeIcon from '@mui/icons-material/DateRange'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import RestaurantIcon from '@mui/icons-material/Restaurant'
import EditLocationAltIcon from '@mui/icons-material/EditLocationAlt'
import MenuItem from '@mui/material/MenuItem'
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import PersonIcon from '@mui/icons-material/Person'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import InventoryIcon from '@mui/icons-material/Inventory'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import AssessmentIcon from '@mui/icons-material/Assessment'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import WarningIcon from '@mui/icons-material/Warning'
import FeedIcon from '@mui/icons-material/Feed'
import VaccinesIcon from '@mui/icons-material/Vaccines'
// facesheet code
import { useReactToPrint } from 'react-to-print'

// import FaceSheet  from './FaceSheet';

import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'

// global variable
import AppContext from '../../AppContext'

import ListAltIcon from '@mui/icons-material/ListAlt'

// axios api
import api from '../../api/api'

// components to include
import Allergies from './table/Allergies'
import Dailylog from './table/Dailylog'
import Diagnosis from './table/Diagnosis'
import Dietary from './table/Dietary'
import Vitals from './table/Vitals'
import Behavior from './table/Behavior'

import Incident from './include/clinical/Incident'
// import Behavior from './include/clinical/Behavior'

import Vital from './include/clinical/Vital'

// components to include for profile tab
import BasicInfo from './include/profile/BasicInfo'
import Contact from './include/profile/Contact'
// tab panel
import PropTypes from 'prop-types'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'

import Popup from './popup/NotificationPopup'
import PopupAll from './popup/PopupAll'
import TabPopup from './popup/TabPopup'
import TablePopup from './popup/TablePopup'

import Typography from '@mui/material/Typography'

import { useNavigate } from 'react-router-dom'

import * as ReactBootstrap from 'react-bootstrap'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

// skeleton
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

import FormControlLabel from '@mui/material/FormControlLabel'
import MedicationIcon from '@mui/icons-material/Medication'
// content loader / skeleton
import ContentLoader from 'react-content-loader'
import Wounds from './include/clinical/Wounds'
import Emar from './include/clinical/Emar'
import Notes from './include/clinical/Notes'
import Orders from './include/clinical/Orders'
import Assessments from './include/clinical/Assessments'
import InsuranceInformation from './include/profile/InsuranceInformation'
import HealthCareDirectives from './include/profile/HealthCareDirectives'
import Routines from './include/profile/Routines'
import Disabilities from './include/profile/Disablities'
import Miscellaneous from './include/profile/Miscellaneous'
import DigitalDocuments from './include/profile/DigitalDocuments'
const blue = {
  500: '#007FFF',
}

const grey = {
  400: '#BFC7CF',
  500: '#AAB4BE',
  600: '#6F7E8C',
}

function TenantProfile() {
  let x = localStorage.getItem('actionValue')
  let y = parseInt(x)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = (val) => {
    setAnchorEl(null)
    console.log(val)
  }

  const helloWorld = () => {
    console.log('hehehe')
  }

  // code for scrolling
  const testRef = useRef(null)
  const scrollToElement = () => testRef.current.scrollIntoView()
  // tab panel
  function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    )
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    }
  }

  const [value, setValue] = React.useState(0)
  let popupIsOpen = localStorage.getItem('popupIsOpen')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const label = { componentsProps: { input: { 'aria-label': 'Demo switch' } } }
  let navigate = useNavigate()
  const componentRef = useRef()

  const [isReachTheEndData, setIsReachTheEndData] = useState(false)
  const [isReachTheStartData, setIsReachTheStartData] = useState(false)

  const [profileSubMenu, setProfileSubMenu] = useState('Basic Information')

  // usestate for loading
  const [loading, setLoading] = useState(false)

  const delayTimer = () => {
    const timer = setTimeout(() => {
      handlePrint()
    }, 500)
    return () => clearTimeout(timer)
  }
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  })
  const [tenantData, setTenantData] = useState({})
  const [currentTab, setCurrentTab] = useState('')
  const [age, setAge] = useState('')
  const {
    diagnosisData,
    setDiagnosisData,
    allergiesList,
    setAllergiesList,
    dialogClose,
    setDialogClose,
    tenantList,
    setTenantList,
    behaviorData,
    setBehaviorData,
    dailylogData,
    setDailylogData,
    dietaryData,
    setDietaryData,
    incidentReportList,
    setIncidentReportList,
    vitalData,
    setVitalData,
    isPrivacyOn,
    setIsPrivacyOn,
    notificationData,
    setNotificationData,
    activeTab,
    setActiveTab,
    isPopupOpen,
    setIsPopupOpen,
    popupChildren,
    setPopupChildren,
  } = useContext(AppContext)

  const [statusInput, setStatusInput] = useState('')
  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')

  const firstUpdate = useRef(true)
  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      handleUpdateStatus()
    }
  }, [statusInput])

  // function for updating tenant's status
  const handleUpdateStatus = () => {
    console.log('changing status. .. .')
    api
      .put(
        `residents/${tenantId}/changeStatus`,
        { status: statusInput },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((result) => {
        // tenantData.forEach(item => {
        //      if(item._id === tenantId){
        //      item.status = statusInput
        //      }
        //    })

        // setting the new data to the state
        setTenantData({ ...tenantData, status: statusInput })

        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // adding history to database
        const input2 = {
          title: `Edited ${tenantName}'s status to ${statusInput}`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  // const fetchTenantHistory = async () => {
  //   await api
  //     .get(`/history/${tenantId}/fetch`, {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     })
  //     .then((res) => {
  //       // setHistoryList(res.data)
  //     })
  // }

  // useEffect(() => {
  //   fetchTenantHistory()
  // }, [])

  const fetchNotification = async () => {
    await api
      .get(`/notification/${tenantId}/fetchAll`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setNotificationData(res.data)
        // setHistoryList(res.data)
      })
  }

  const sortedNotification = notificationData.sort((a, b) => b.priorityLevel - a.priorityLevel)

  useEffect(() => {
    fetchNotification()
  }, [tenantId])

  let fullName = tenantData.firstName + ' ' + tenantData.middleName + ' ' + tenantData.lastName

  useEffect(() => {
    setCurrentTab('profile')
    // localStorage.setItem('tenantId' , tenantData._id);
    // localStorage.setItem('tenantName' , fullName);
  }, [])
  // code for fetching allergy

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const getTheAge = (birthday) => {
    let date = new Date(birthday)
    let currentYear = new Date().getFullYear()
    let year = date.getUTCFullYear()

    const age = currentYear - year

    return age
  }

  // function for fetchning the name of the tenant and setting it to the localstorage
  const setTenantName = (id) => {
    api
      .get(`/residents/${id}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        let fullName = res.data.firstName + ' ' + res.data.middleName + ' ' + res.data.lastName

        localStorage.setItem('tenantName', fullName)
      })
  }

  useEffect(() => {
    fetchBehaviorData()
  }, [tenantId])

  const fetchBehaviorData = () => {
    let token = localStorage.getItem('token')
    api
      .get(`/behavior/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const newFilter = res.data.filter((val) => {
          return val.isActive === true
        })
        setBehaviorData(newFilter)
      })
  }

  const fetchAllergy = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/allergy/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  useEffect(() => {
    fetchAllTenant()
  }, [])

  const fetchAllTenant = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/residents/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setTenantList(res.data)
      })
  }

  useEffect(() => {
    const getAllergyList = async () => {
      const allAlergy = await fetchAllergy()
      if (allAlergy) setAllergiesList(allAlergy)
    }

    getAllergyList()
  }, [tenantId])

  const fetchTenant = async () => {
    let tenantId = localStorage.getItem('tenantId')
    let token = localStorage.getItem('token')
    const res = await api.get(`/residents/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  useEffect(() => {
    const getTenantData = async () => {
      const tenantDetails = await fetchTenant()
      if (tenantDetails) setTenantData(tenantDetails)
    }

    getTenantData()
  }, [tenantId])

  console.log(tenantData)

  const fetchDiagnosisData = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/diagnosis/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  useEffect(() => {
    const getDiagnosisData = async () => {
      const allDiagnosis = await fetchDiagnosisData()
      if (allDiagnosis) setDiagnosisData(allDiagnosis)
    }

    getDiagnosisData()
  }, [tenantId])

  const changeCurrentTab = (profileVal, subMenu) => {
    setCurrentTab('profile')
    // setProfileTab(profileVal)
    setProfileSubMenu(subMenu)
  }

  let tenantCurrentIndex = localStorage.getItem('tenantCurrentIndex')
  useEffect(() => {
    const search = (obj) => obj.id == tenantId
    const index = tenantList.findIndex(search)
    localStorage.setItem('tenantCurrentIndex', index)
    if (index == tenantList.length - 1) {
      setIsReachTheEndData(true)
    } else {
      setIsReachTheEndData(false)
    }

    if (index == 0) {
      setIsReachTheStartData(true)
    } else {
      setIsReachTheStartData(false)
    }
  }, [tenantCurrentIndex])
  // function for next and previous button
  const nextButtonFunction = () => {
    const search = (obj) => obj.id == tenantId
    setLoading(false)
    const index = tenantList.findIndex(search)
    localStorage.setItem('tenantCurrentIndex', index + 1)
    const newIndex = tenantList[index + 1].id
    setTenantName(newIndex)
    // set new id in the localstorage
    localStorage.setItem('tenantId', newIndex)
    navigate(`/resident/${newIndex}`)
    fetchTenant(newIndex)
    fetchAllergy()
  }

  const prevButtonFunction = () => {
    const search = (obj) => obj.id == tenantId
    setLoading(false)
    const index = tenantList.findIndex(search)

    localStorage.setItem('tenantCurrentIndex', index - 1)
    const newIndex = tenantList[index - 1].id
    setTenantName(newIndex)
    // set new id in the localstorage
    localStorage.setItem('tenantId', newIndex)
    navigate(`/resident/${newIndex}`)
    fetchTenant(newIndex)
    fetchAllergy()
  }

  const formatDate = (string) => {
    if (string === undefined) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' })
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const fetchDailylogData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/dailylog/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setDailylogData(res.data)
      })
  }

  console.count('Render:')
  console.log(isPopupOpen)

  useEffect(() => {
    console.log('hello world')
    fetchDailylogData()
  }, [tenantId])

  const fetchDietaryData = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get(`/dietary/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data)
        setDietaryData(res.data)
      })
  }

  useEffect(() => {
    fetchDietaryData()
  }, [tenantId])

  const fetchIncidentReport = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/incident/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setIncidentReportList(res.data)
      })
  }

  useEffect(() => {
    fetchIncidentReport()
  }, [tenantId])

  const fetchVitalData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/vitals/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setVitalData(res.data)
      })
  }

  useEffect(() => {
    fetchVitalData()
  }, [tenantId])

  console.log(vitalData)

  const changeActiveTab = (activeTab) => {
    setActiveTab(activeTab)
    // scrollToElement()
  }

  const resetActiveTab = () => {
    setActiveTab(0)
  }

  const scrollDown = () => {
    window.scrollTo({
      top: 1000,
      behavior: 'smooth',
    })
  }

  console.log(tenantData)

  // function for setting the value of notification id to be fetch
  const setNotificationId = (id) => {
    localStorage.setItem('notificationId', id)
  }

  return (
    <div>
      <TabPopup />
      {isPrivacyOn == false ? (
        <div>
          <div
            onClick={() => nextButtonFunction()}
            id={isReachTheEndData ? 'hide-element' : 'next-btn'}
          >
            <KeyboardDoubleArrowRightIcon id="next-icon" />
          </div>
          <div
            onClick={() => prevButtonFunction()}
            id={isReachTheStartData ? 'hide-element' : 'prev-btn'}
          >
            <KeyboardDoubleArrowLeftIcon id="prev-icon" />
          </div>
        </div>
      ) : null}

      {isPrivacyOn == false ? (
        <>
          <Grid md={12} container rowSpacing={2}>
            <Grid
              item
              md={9}
              sm={12}
              xs={12}
              direction="column"
              style={{
                display: 'flex',
                marginBottom: '0px',
                paddingTop: '0px',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Grid md={12} container rowSpacing={2} columnSpacing={{ xs: 2, sm: 2, md: 2 }}>
                <Grid
                  item
                  md={12}
                  sm={12}
                  xs={12}
                  className="mt-3 mb-2"
                  direction="column"
                  style={{
                    display: 'flex',
                    marginBottom: '0px',
                    paddingLeft: '10px',
                    paddingTop: '0px',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <div id="container" className="position-relative">
                    <Grid container rowSpacing={2}>
                      <Grid
                        item
                        md={3}
                        sm={12}
                        xs={12}
                        direction="column"
                        style={{
                          display: 'flex',
                          marginTop: '15px',
                          marginBottom: '15px',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <div id="card-bgc2">
                          <div id="tenant-image-holder2">
                            <img id="tenant-image" src={tenantData.picture} />
                          </div>
                        </div>
                      </Grid>
                      <Grid
                        id="tenant-profile-data"
                        className="mt-4 mb-3"
                        item
                        md={5}
                        sm={12}
                        xs={12}
                        direction="column"
                        style={{
                          marginLeft: '15px',
                        }}
                      >
                        <div>
                          <div>
                            <h4
                              className="name"
                              style={{
                                padding: '0px',
                                margin: '0px',
                              }}
                            >
                              {tenantName}
                            </h4>
                          </div>
                          <div>
                            <p className="body-text room">{tenantData.communityName}</p>
                          </div>
                          <div>
                            <p className="body-text carelevel">
                              Care Level : {tenantData.careLevel}
                            </p>
                          </div>
                        </div>
                      </Grid>
                      <Grid
                        className="mt-2 mb-3"
                        item
                        md={3}
                        sm={12}
                        xs={12}
                        direction="column"
                        style={{
                          marginLeft: '15px',
                        }}
                      >
                        <div className="btn mt-2 position-relative">
                          Status : {tenantData.status}
                          <PopupAll title="Change Status">
                            <div id="status-holder" style={{ width: '250px', padding: '0px' }}>
                              {' '}
                              Status: {tenantData.status}
                            </div>
                            <Box id="drop-Drown2" sx={{ minWidth: 150 }}>
                              <FormControl>
                                <InputLabel
                                  variant="standard"
                                  htmlFor="uncontrolled-native"
                                ></InputLabel>
                                <NativeSelect
                                  value={statusInput}
                                  onChange={(e) => setStatusInput(e.target.value)}
                                  style={{ fontSize: '18px', width: '100%' }}
                                >
                                  <option value="Cancel Move in">Cancel Move in</option>
                                  <option value="Move Out">Move Out</option>
                                  <option value="On Notice">On Notice</option>
                                  <option value="Start LOA">Start LOA</option>
                                  <option value="Put On Waitlist">Put On Waitlist</option>
                                  <option value="Unit/Care Level Change">
                                    Unit/Care Level Change
                                  </option>
                                  <option value="Schedule Unit Transfer">
                                    Schedule Unit Transfer
                                  </option>
                                </NativeSelect>
                              </FormControl>
                            </Box>
                          </PopupAll>
                          {/* <Box id="drop-Drown2" sx={{ minWidth: 150 }}>
                            <FormControl>
                              <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                <div>
                                  <strong style={{ fontSize: '13px', width: '100%' }}>
                                    Status : {tenantData.status}
                                    <PopupAll>yowww</PopupAll>
                                  </strong>
                                </div>
                              </InputLabel>
                              <NativeSelect
                                value={statusInput}
                                onChange={(e) => setStatusInput(e.target.value)}
                                style={{ fontSize: '12px', width: '100%' }}
                              >
                                <option value="Cancel Move in">Cancel Move in</option>
                                <option value="Move Out">Move Out</option>
                                <option value="On Notice">On Notice</option>
                                <option value="Start LOA">Start LOA</option>
                                <option value="Put On Waitlist">Put On Waitlist</option>
                                <option value="Unit/Care Level Change">
                                  Unit/Care Level Change
                                </option>
                                <option value="Schedule Unit Transfer">
                                  Schedule Unit Transfer
                                </option>
                              </NativeSelect>
                            </FormControl>
                          </Box> */}
                        </div>
                      </Grid>
                    </Grid>
                  </div>
                </Grid>

                <Grid
                  id="allergy-holder-tablet"
                  item
                  md={6}
                  sm={12}
                  xs={12}
                  direction="column"
                  style={{
                    display: 'flex',
                    marginBottom: '0px',
                    paddingTop: '0px',
                    paddingLeft: '10px',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <div id="container3" className="position-relative">
                    <Grid
                      className="mt-2 position-relative"
                      item
                      md={12}
                      sm={12}
                      xs={12}
                      direction="column"
                      style={{
                        display: 'flex',
                        justifyContent: 'left',
                        alignItems: 'left',
                      }}
                    >
                      <div className=" mx-auto">
                        {allergiesList.length === 0 ? null : (
                          <strong
                            style={{
                              fontSize: '20px',
                            }}
                          >
                            Allergies
                          </strong>
                        )}
                      </div>
                      <div id="list-item-holder" className="mx-auto">
                        {allergiesList.length === 0 ? (
                          <strong className="d-inline-block pl-2"> No Allergy Data yet</strong>
                        ) : null}
                        <ul className="d-block">
                          {allergiesList.map((item, key) => (
                            <p className="body-text"> • {item.allergy}</p>
                          ))}
                        </ul>
                      </div>
                    </Grid>
                  </div>
                </Grid>
                <Grid
                  id="allergy-holder-tablet"
                  item
                  md={6}
                  sm={12}
                  xs={12}
                  direction="column"
                  style={{
                    display: 'flex',
                    marginBottom: '0px',
                    paddingTop: '0px',
                    paddingLeft: '10px',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <div id="container3" className="position-relative">
                    <Grid
                      className="position-relative"
                      item
                      md={12}
                      sm={12}
                      xs={12}
                      direction="column"
                      style={{
                        display: 'flex',
                        justifyContent: 'left',
                        alignItems: 'left',
                      }}
                    >
                      <div className=" mx-auto">
                        {diagnosisData.length === 0 ? null : (
                          <strong
                            style={{
                              fontSize: '20px',
                            }}
                          >
                            Diagnosis
                          </strong>
                        )}
                      </div>
                      <div id="list-item-holder" className=" mx-auto">
                        {diagnosisData.length === 0 ? (
                          <strong className="d-inline-block pl-2"> No Diagnosis Data yet</strong>
                        ) : null}
                        <ul className="d-block">
                          {diagnosisData.map((item, key) => (
                            <p className="body-text"> • {item.diagnosisName}</p>
                          ))}
                        </ul>
                      </div>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              md={3}
              sm={12}
              xs={12}
              direction="column"
              style={{
                paddingTop: '0px',
              }}
            >
              {' '}
              <div id="container2" className="position-relative">
                <div>
                  <strong
                    style={{
                      fontSize: '20px',
                      marginLeft: '20px',
                    }}
                  >
                    Notification/Action
                  </strong>
                  {/* Insert notification data here */}
                  <div>
                    {sortedNotification.length == 0 ? (
                      <strong className="d-inline-block pl-2">
                        {' '}
                        No Notification Data is Available
                      </strong>
                    ) : null}
                    {sortedNotification.map((item, key) => (
                      <div
                        key={key}
                        id="notification-holder"
                        className={item.isCompleted == false ? '' : 'disabled-btn'}
                        onClick={(e) => setNotificationId(item.id)}
                      >
                        <Popup data={item} />
                        {item.isCompleted == false ? (
                          <p className="text-right my-2">
                            {' '}
                            <FontAwesomeIcon
                              icon={faTriangleExclamation}
                              style={{ fontSize: '18px', color: '#f9b115' }}
                            />{' '}
                            This needs an attention.
                          </p>
                        ) : (
                          <p className="text-right my-2">
                            {' '}
                            <FontAwesomeIcon
                              icon={faCircleCheck}
                              style={{ fontSize: '18px', color: '#07BC0C' }}
                            />{' '}
                            Done.
                          </p>
                        )}
                        <p className="body-text d-inline-block"> {item.title}</p>
                        {item.isCompleted == false ? (
                          <p className="body-text d-inline-block pb-2"> {formatDate(item.date)}</p>
                        ) : (
                          <>
                            <div className="body-text d-inline-block">value: {item.value}</div>
                            <p className="body-text d-inline-block pb-2">
                              {' '}
                              Date Acoomplish:
                              {formatDate(item.dateAccomplish)}
                            </p>
                          </>
                        )}
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>

          <div id="container" className="position-relative pb-5 mt-2 ">
            <Box sx={{ width: '100%' }}>
              <Box
                className="position-relative mx-auto"
                sx={{ borderBottom: 1, borderColor: 'divider', width: '95%' }}
              >
                <Tabs
                  variant="scrollable"
                  scrollButtons="false"
                  aria-label="scrollable auto tabs example"
                  value={value}
                  onChange={handleChange}
                  aria-label="basic tabs example"
                >
                  <Tab style={{ fontSize: '10px' }} label="Clinical" {...a11yProps(0)} />
                  <Tab
                    style={{ fontSize: '10px' }}
                    label="Profile"
                    {...a11yProps(1)}
                    onClick={(e) => resetActiveTab()}
                  />
                  <Tab
                    style={{ fontSize: '10px' }}
                    label="Reports"
                    {...a11yProps(2)}
                    onClick={(e) => resetActiveTab()}
                  />
                  <Tab
                    style={{ fontSize: '10px' }}
                    label="e-Documents"
                    {...a11yProps(3)}
                    onClick={(e) => resetActiveTab()}
                  />
                </Tabs>
              </Box>
              <Box
                className="mx-auto"
                sx={{ borderBottom: 1, borderColor: 'divider', width: '95%' }}
              >
                {/* Clinical Submenu */}
                {value == 0 ? (
                  <Tabs
                    id="sub-menu"
                    variant="scrollable"
                    scrollButtons="false"
                    aria-label="scrollable auto tabs example"
                  >
                    {/* <div>
                      <Tab
                        className={activeTab == 1 ? 'active-menu sub-menu-item' : 'sub-menu-item  '}
                        label="Allergies"
                        onClick={() => changeActiveTab(1)}
                        style={{ fontSize: '12px' }}
                      />
                    </div> */}

                    {/* <Tab
                      className={activeTab == 2 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Behavior"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(2)}
                    /> */}

                    {/* <Tab
                      className={activeTab == 3 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Daily Log"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(3)}
                    /> */}
                    {/* <Tab
                      className={activeTab == 4 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Diagnosis"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(4)}
                    /> */}
                    {/* <Tab
                      className={activeTab == 7 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Dietary"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(7)}
                    /> */}
                    {/* <Tab
                      className={activeTab == 5 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Incident"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(5)}
                    />
                    <Tab
                      className={activeTab == 6 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Vitals"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(6)}
                    /> */}
                    {/* <Tab
                      className={activeTab == 8 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="e-MAR"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(8)}
                    /> */}
                    <Tab
                      className={
                        activeTab == 9
                          ? 'active-menu sub-menu-item'
                          : 'sub-menu-item position-relative'
                      }
                      style={{ fontSize: '12px', width: '25px' }}
                      label="Wounds"
                      onClick={() => {
                        changeActiveTab(9)
                        setIsPopupOpen(true)
                        setPopupChildren('wounds')
                      }}
                    />
                    <Tab
                      className={activeTab == 10 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Progress Note"
                      style={{ fontSize: '12px' }}
                      onClick={() => {
                        changeActiveTab(10)
                        setIsPopupOpen(true)
                        setPopupChildren('note')
                      }}
                    />
                    {/* <Tab
                      className={activeTab == 11 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Orders"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(11)}
                    />
                    <Tab
                      className={activeTab == 12 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      label="Assessments"
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(12)}
                    /> */}
                    <Tab
                      className={
                        activeTab == 12
                          ? 'active-menu sub-menu-item'
                          : 'sub-menu-item position-relative'
                      }
                      style={{ fontSize: '12px', width: '25px' }}
                      label="Assessment"
                      onClick={() => {
                        changeActiveTab(12)
                        setIsPopupOpen(true)
                        setPopupChildren('assessment')
                      }}
                    />
                    <Tab
                      className={
                        activeTab == 20
                          ? 'active-menu sub-menu-item'
                          : 'sub-menu-item position-relative'
                      }
                      style={{ fontSize: '12px', width: '25px' }}
                      label="Care Plan"
                      onClick={() => {
                        changeActiveTab(20)
                        setIsPopupOpen(true)
                        setPopupChildren('careplan')
                      }}
                    />
                  </Tabs>
                ) : value == 1 ? (
                  // profile sub menu
                  <Tabs id="sub-menu" aria-label="basic tabs example">
                    <Tab
                      className={activeTab == 20 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(20)}
                      label="Basic Info"
                    />
                    <Tab
                      className={activeTab == 21 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(21)}
                      label="Contacts"
                    />
                    <Tab
                      className={activeTab == 22 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(22)}
                      label="Insurance Information"
                    />
                    <Tab
                      className={activeTab == 23 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(23)}
                      label="Health Care Directives"
                    />
                    <Tab
                      className={activeTab == 24 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(24)}
                      label="Routines"
                    />{' '}
                    <Tab
                      className={activeTab == 25 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(25)}
                      label="Disablities"
                    />
                    <Tab
                      className={activeTab == 26 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(26)}
                      label="Miscellaneous"
                    />
                    <Tab
                      className={activeTab == 27 ? 'active-menu sub-menu-item' : 'sub-menu-item'}
                      style={{ fontSize: '12px' }}
                      onClick={() => changeActiveTab(27)}
                      label="Digital Documents"
                    />
                  </Tabs>
                ) : null}
              </Box>
              <TabPanel value={value} index={0}>
                {/* cards code start here */}
                <CContainer>
                  <CRow xs={{ gutterY: 3 }}>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem', zIndex: '1' }}
                      >
                        <CardActionArea>
                          <PopupAll title="Allergies">
                            <Allergies activeTabVal="1" />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Allergies
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {allergiesList.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {allergiesList[allergiesList.length - 1].allergy}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest allergy data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(allergiesList[allergiesList.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(2)}>
                          <PopupAll title="Behavior">
                            <Behavior />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Behavior
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {behaviorData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {behaviorData[behaviorData.length - 1].behavior}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest behavior data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(behaviorData[behaviorData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(3)}>
                          <PopupAll title="Daily log">
                            <Dailylog />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Daily log
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {dailylogData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {dailylogData[dailylogData.length - 1].title}</p>
                                  <p>{dailylogData[dailylogData.length - 1].note}</p>
                                  <p>
                                    Created by: {dailylogData[dailylogData.length - 1].userName}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest log data of the resident
                                  </small>
                                  <p>
                                    Issued Date:{' '}
                                    {formatDate(dailylogData[dailylogData.length - 1].date)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(4)}>
                          <PopupAll title="Diagnosis">
                            <Diagnosis />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Diagnosis
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {diagnosisData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {diagnosisData[diagnosisData.length - 1].diagnosisName}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest diagnosis data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(diagnosisData[diagnosisData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(7)}>
                          <PopupAll title="Dietary">
                            <Dietary />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Dietary
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {dietaryData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {dietaryData[dietaryData.length - 1].dietaryPreference}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest dietary data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(dietaryData[dietaryData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(5)}>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Incident
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {incidentReportList.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    {' '}
                                    {
                                      incidentReportList[incidentReportList.length - 1]
                                        .incidentDescription
                                    }
                                  </p>
                                  <p>
                                    Location:{' '}
                                    {incidentReportList[incidentReportList.length - 1].location}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest incident data of the resident
                                  </small>
                                  <p>
                                    Date Happened:{' '}
                                    {formatDate(
                                      incidentReportList[incidentReportList.length - 1].date,
                                    )}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(6)}>
                          <PopupAll title="Vitals">
                            <Vitals />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Vitals
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {vitalData.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    Blood Pressure: {vitalData[vitalData.length - 1].systolic}/
                                    {vitalData[vitalData.length - 1].diastolic}
                                  </p>
                                  <p>
                                    Body Temperature: {vitalData[vitalData.length - 1].bodyTemp}
                                  </p>
                                  <p>Pulse Rate: {vitalData[vitalData.length - 1].pulseRate}</p>
                                  <p>
                                    Respiration Rate:{' '}
                                    {vitalData[vitalData.length - 1].respirationRate}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest Vital data of the resident
                                  </small>
                                  <p>
                                    Date Input: {formatDate(vitalData[vitalData.length - 1].date)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                  </CRow>
                </CContainer>
              </TabPanel>
              <TabPanel value={value} index={1}>
                <CContainer>
                  <CRow xs={{ gutterY: 3 }}>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent className="position-relative">
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Basic Info
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Contacts
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Insurance Information
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Health Care Directives
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Routines
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Disabilities
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Miscellaneous
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Digital Documents
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                  </CRow>
                </CContainer>
              </TabPanel>
              <TabPanel value={value} index={2}>
                Reports
              </TabPanel>
              <TabPanel value={value} index={3}>
                e-Documents
              </TabPanel>
              <TabPanel value={value} index={4}>
                Contacts
              </TabPanel>
              <TabPanel value={value} index={5}>
                Insurance
              </TabPanel>
              <TabPanel value={value} index={6}>
                Healthcare
              </TabPanel>
              <TabPanel value={value} index={7}>
                Routines
              </TabPanel>
              <TabPanel value={value} index={8}>
                Medical History
              </TabPanel>
              <TabPanel value={value} index={9}>
                Surgeries
              </TabPanel>
              <TabPanel value={value} index={10}>
                Disabilities
              </TabPanel>
              <TabPanel value={value} index={11}>
                Miscellaneous
              </TabPanel>
              <TabPanel value={value} index={12}>
                Digital Documents
              </TabPanel>
            </Box>
          </div>
          {/* <div ref={testRef}>
            {activeTab == 2 ? (
              <div className="mx-auto">
                <Behavior />
              </div>
            ) : activeTab == 5 ? (
              <div className="mx-auto">
                <Incident />
              </div>
            ) : activeTab == 6 ? (
              <div className="mx-auto">
                <Vital />
              </div>
            ) : activeTab == 7 ? (
              <div className="mx-auto">
                <Dietary />
              </div>
            ) : activeTab == 8 ? (
              <div className="mx-auto">
                <Emar />
              </div>
            ) : activeTab == 9 ? (
              <div className="mx-auto">
                <Wounds />
              </div>
            ) : activeTab == 10 ? (
              <div className="mx-auto">
                <Notes />
              </div>
            ) : activeTab == 11 ? (
              <div className="mx-auto">
                <Orders />
              </div>
            ) : activeTab == 12 ? (
              <div className="mx-auto">
                <Assessments />
              </div>
            ) : activeTab == 20 ? (
              <div className="mx-auto">
                <BasicInfo />
              </div>
            ) : activeTab == 21 ? (
              <div className="mx-auto">
                <Contact />
              </div>
            ) : activeTab == 22 ? (
              <div className="mx-auto">
                <InsuranceInformation />
              </div>
            ) : activeTab == 23 ? (
              <div className="mx-auto">
                <HealthCareDirectives />
              </div>
            ) : activeTab == 24 ? (
              <div className="mx-auto">
                <Routines />
              </div>
            ) : activeTab == 25 ? (
              <div className="mx-auto">
                <Disabilities />
              </div>
            ) : activeTab == 26 ? (
              <div className="mx-auto">
                <Miscellaneous />
              </div>
            ) : activeTab == 27 ? (
              <div className="mx-auto">
                <Miscellaneous />
              </div>
            ) : activeTab == 28 ? (
              <div className="mx-auto">
                <DigitalDocuments />
              </div>
            ) : null}
          </div> */}
        </>
      ) : (
        <div id="tenant-profile-loader">
          <ContentLoader
            speed={2}
            viewBox="0 0 476 124"
            backgroundColor={'#3333'}
            foregroundColor={'#999'}
          >
            <rect x="97" y="56" rx="3" ry="3" width="410" height="6" />
            <rect x="97" y="75" rx="3" ry="3" width="380" height="6" />
            <rect x="97" y="90" rx="3" ry="3" width="178" height="6" />
            <rect x="4" y="16" rx="0" ry="0" width="87" height="83" />
            <rect x="97" y="18" rx="3" ry="3" width="380" height="6" />
            <rect x="97" y="37" rx="3" ry="3" width="217" height="6" />
          </ContentLoader>
          <ContentLoader
            speed={2}
            viewBox="0 0 476 124"
            backgroundColor={'#3333'}
            foregroundColor={'#999'}
          >
            <rect x="53" y="112" rx="0" ry="0" width="0" height="5" />
            <rect x="5" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="52" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="99" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="146" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="193" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="240" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="287" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="334" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="381" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="428" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="9" y="52" rx="0" ry="0" width="458" height="8" />
            <rect x="10" y="72" rx="0" ry="0" width="266" height="5" />
            <rect x="10" y="88" rx="0" ry="0" width="357" height="6" />
          </ContentLoader>
        </div>
      )}

      {/* <Dietary /> */}

      <Row>
        {/*<div style={{ display: "none" }}>
              <FaceSheet ref={componentRef} />
            </div> */}

        {/*insert action button tab here*/}

        <Col md="12" className="d-flex justify-content-center">
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </Col>
      </Row>
    </div>
  )
}

export default TenantProfile
