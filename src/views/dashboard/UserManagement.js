import React, { useEffect, useState, useContext } from 'react'
import api from '../../api/api'
import { Row, Col, Table } from 'react-bootstrap'
import TextField from '@mui/material/TextField'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import Autocomplete from '@mui/material/Autocomplete'
import EditPopup from './popup/EditPopup'
import Popup from './popup/Popup'
import { DataGrid } from '@mui/x-data-grid'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardHeader,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CCol,
  CRow,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import AddNewUser from './form/AddNewUser'
import AppContext from '../../AppContext'

function UserManagement() {
  const [isFetchDone, setIsFetchDone] = useState(false)
  const { userList, setUserList, allergiesList, setAllergiesList, currentTab, setCurrentTab } =
    useContext(AppContext)
  const fullName = (data) => {
    return data.lastName + ',' + data.firstName + data.middleName
  }
  const columns = [
    {
      field: 'email',
      headerName: 'Email',
      maxWidth: 250,
      flex: 0.5,
    },
    {
      field: 'username',
      headerName: 'Username',
      maxWidth: 250,
    },
    {
      field: 'fullName',
      headerName: 'Name',
      maxWidth: 250,
      sortable: false,
      flex: 0.5,
      valueGetter: (params) => {
        return fullName(params.row)
      },
    },
    {
      field: 'userType',
      headerName: 'User Type',
      maxWidth: 250,
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>hehehe</EditPopup>
          <div id="delete-btn" className="btn d-flex">
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  // code for fetching allergy
  useEffect(() => {
    fetchUserList()
  }, [])

  const fetchUserList = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    let email = localStorage.getItem('email')
    api
      .get(`/users/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        // add filter to return all the users except the user that are logged in
        const newFilter = res.data.filter((val) => {
          return val.email != email
        })
        setUserList(newFilter)
        setIsFetchDone(true)
      })
  }
  return (
    <CCard>
      <CCardHeader id="card-header">
        <strong id="res-title">User Management</strong>
      </CCardHeader>
      <CCardBody>
        <Popup id="popup">
          <AddNewUser />
        </Popup>
        <CRow xs={{ gutterY: 3 }}>
          <div className="mt-3 mx-auto" id="datagrid-container">
            <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-5">
              {userList.length > 0 && isFetchDone == true ? (
                <DataGrid
                  getRowId={(row) => row.id}
                  rows={userList}
                  columns={columns}
                  pageSize={5}
                  rowsPerPageOptions={[5]}
                />
              ) : (
                <p>No Available Data</p>
              )}
            </Col>
          </div>
        </CRow>
      </CCardBody>
    </CCard>
  )
}

export default UserManagement
