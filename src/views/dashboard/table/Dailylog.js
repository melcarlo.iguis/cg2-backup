import React, { useState, useContext, forwardRef, useRef } from 'react'
import MaterialTable from 'material-table'
import AppContext from '../../../AppContext'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
// axios api
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function Dailylog() {
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    dailylogData,
    setDailylogData,
  } = useContext(AppContext)
  const [data, setData] = useState([])

  const cutDate = (string) => {
    console.log(string)
    let date = new Date(string)
    console.log(date)
    // const month = date.toLocaleString('en-us', { month: 'long' }); /* June */

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    const hour = date.getHours()
    const min = date.getMinutes()

    const newMins = min < 10 ? '0' + min : min
    var ampm = date.getHours() >= 12 ? 'PM' : 'AM'
    const newDay = day < 10 ? '0' + day : day

    let fullDate = month + '/' + newDay + '/' + year + ' - ' + hour + ':' + newMins + ' ' + ampm

    return fullDate
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  const allergyTypeSelection = { 1: 'Food', 2: 'Environment' }

  const columns = [
    {
      title: 'Date and Time',
      field: 'date',
      type: 'datetime',
      dateSetting: { locale: 'en-US' },
      render: (rowData) => cutDate(rowData.date),
    },
    {
      title: 'Title',
      field: 'title',
    },
    { title: 'Name of the Staff', field: 'userName' },
  ]

  //   const columns = [
  //     {
  //       field: 'date',
  //       headerName: 'Date and Time',
  //       flex: 0.5,
  //       width: 120,
  //       valueGetter: (params) => {
  //         return cutDate(params.row.date)
  //       },
  //     },
  //     {
  //       field: 'title',
  //       headerName: 'Title',
  //       flex: 2,
  //       width: 180,
  //     },

  //     {
  //       field: 'userName',
  //       headerName: 'Name of the Staff',
  //       flex: 0.5,
  //       minWidth: 200,
  //     },
  //     {
  //       field: 'action',
  //       headerName: 'Action',
  //       sortable: false,
  //       flex: 0.5,
  //       minWidth: 80,
  //       renderCell: (params) => (
  //         <div className="d-flex">
  //           <EditPopup>
  //             <EditObservationForm data={params.row} />
  //           </EditPopup>
  //           <div
  //             id="delete-btn"
  //             className="btn d-flex"
  //             onClick={(e) => deleteObservationData(e, params.row.id)}
  //           >
  //             <DeleteForeverIcon />
  //           </div>
  //         </div>
  //       ),
  //     },
  //   ]

  const addAllergy = (data) => {
    console.log(data)
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const input = {
      allergy: data.allergy,
      allergyType: data.allergyType,
      allergenType: data.allergenType,
      startDate: data.startDate,
      endDate: data.endDate,
      ResidentId: tenantId,
    }

    console.log(input)
    // api call for adding new allergy tenant
    api
      .post(`/allergy/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setAllergiesList([...allergiesList, result.data])
        // code for adding shiftlog summary
        // const shiftSummaryLogInput = {
        //   activity: `${name} added ${allergy} as a new allergy data to ${tenantName}`,
        //   residentName: tenantName,
        //   userName: name,
        //   ResidentId: tenantId,
        //   UserId: userId,
        // }
        // console.log(shiftSummaryLogInput)
        // api
        //   .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {
        //     // setDailylogData([...dailylogData, result.data])
        //     console.log(result)
        //   })
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
    // code for adding daily log
    // const dailyLogInput = {
    //   title: `${name} added ${allergy} as a new allergy data to ${tenantName}`,
    //   residentName: tenantName,
    //   userName: name,
    //   ResidentId: tenantId,
    // }
    // api
    //   .post(`/dailylog/add`, dailyLogInput, {
    //     headers: {
    //       Authorization: `Bearer ${token}`,
    //     },
    //   })
    //   .then((result) => {
    //     setDailylogData([...dailylogData, result.data])
    //     console.log(result)
    //   })
    //   .catch((err) => {
    //     console.error(err)
    //   })
  }

  const deleteObservationData = (id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    api
      .delete(`/observation/${tenantId}/delete/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        const newArr = [...dailylogData]

        newArr.forEach((item, index) => {
          if (item._id === id) {
            newArr.splice(index, 1)
          }
        })

        setDailylogData(newArr)
        Swal.fire('Deleted!', 'The Data has been deleted.', 'success')

        // adding history to database
        const input2 = {
          title: `Deleted ${tenantName}'s observation data`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {})
          .catch((err) => {
            console.error(err)
          })
      })
      .catch((err) => {
        Swal.fire({
          title: 'fail to delete',
          icon: 'error',
        })
      })
  }

  const deleteAllergy = (id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    console.log(id)

    api
      .put(`/allergy/${id}/delete`)
      .then((result) => {
        console.log(result)

        const newArr = [...allergiesList]
        newArr.forEach((item, index) => {
          if (item.id === id) {
            newArr.splice(index, 1)
          }
        })

        setAllergiesList(newArr)

        // adding history to database
        // const input2 = {
        //   title: `Deleted ${tenantName}'s allergy data on ${allergy}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Deleted Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'fail to delete',
          icon: 'error',
        })
      })
  }

  const updateAllergy = (data, id) => {
    console.log(data)
    console.log(id)
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      allergy: data.allergy,
      allergyType: data.allergyType,
      allergenType: data.allergenType,
      startDate: data.startDate,
      endDate: data.endDate,
      reaction: data.reaction,
    }

    // api call for editing allergy data
    api
      .put(`/allergy/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // updating the tenant's allergy data state
        allergiesList.forEach((item) => {
          if (item.id === id) {
            item.allergy = data.allergy
            item.allergyType = data.allergyType
            item.allergenType = data.allergenType
            item.startDate = data.startDate
            item.endDate = data.endDate
            item.reaction = data.reaction
          }
        })

        setAllergiesList([...allergiesList])

        // adding history to database
        // const input2 = {
        //   title: `Edited ${tenantName}'s allergy data on ${allergy}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const updateObservationData = (data, id) => {
    // e.preventDefault()
    // const dateFormated = moment(date).format('YYYY-MM-DD')
    // const dateTime = moment(dateFormated + ' ' + time)

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      date: data.date,
      title: data.title,
    }

    // api call for adding new tenant
    api
      .put(`/dailylog/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)

        dailylogData.forEach((item) => {
          if (item.id === id) {
            item.date = data.date
            item.title = data.title
          }
        })

        setDailylogData([...dailylogData])

        // // adding history to database
        // const input2 = {
        //   title: `Edited ${tenantName}'s Obervation Data`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })

        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div style={{ maxWidth: '100%' }}>
      <MaterialTable
        icons={tableIcons}
        title="Daily Log"
        style={{ zIndex: 9999999 }}
        columns={columns}
        data={dailylogData}
        editable={
          {
            // onRowAdd: (newRow) =>
            //   new Promise((resolve, reject) => {
            //     setTimeout(() => {
            //       setDailylogData([...dailylogData, newRow])
            //       addAllergy(newRow)
            //       resolve()
            //     }, 2000)
            //   }),
            // onRowDelete: (selectedRow) =>
            //   new Promise((resolve, reject) => {
            //     console.log(selectedRow)
            //     setTimeout(() => {
            //       deleteObservationData(selectedRow.id)
            //       resolve()
            //     }, 2000)
            //   }),
            // onRowUpdate: (updatedRow, oldRow) =>
            //   new Promise((resolve, reject) => {
            //     setTimeout(() => {
            //       updateObservationData(updatedRow, oldRow.id)
            //       resolve()
            //     }, 2000)
            //   }),
          }
        }
        options={{
          search: false,
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            zIndex: 9999999,
          },
        }}
      />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default Dailylog
