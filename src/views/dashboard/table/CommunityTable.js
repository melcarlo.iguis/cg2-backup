import React, { useState, useContext, forwardRef, useRef, useEffect } from 'react'
import MaterialTable from 'material-table'
import AppContext from '../../../AppContext'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import CancelIcon from '@mui/icons-material/Cancel'
// axios api
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function Products() {
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    activeTab,
    setActiveTab,
    dailylogData,
    setDailylogData,
  } = useContext(AppContext)
  const [data, setData] = useState([])
  const [communityData, setCommunityData] = useState([])
  const [isFetchDone, setIsFetchDone] = useState(false)

  const formatDate = (string) => {
    if (string === undefined || string === '' || string === null) {
      return string
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate() + 1
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  useEffect(() => {
    fetchCommunityList()
  }, [])

  const fetchCommunityList = () => {
    let token = localStorage.getItem('token')

    api
      .get('/community/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setCommunityData(result.data)
        setIsFetchDone(true)
      })
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  const allergyTypeSelection = { 1: 'Food', 2: 'Environment' }

  const checkIfOccupied = (isOccupied) => {
    if (isOccupied) {
      return (
        <div>
          <CancelIcon />
          Not availble
        </div>
      )
    } else {
      return (
        <div>
          <CheckCircleIcon />
          Available
        </div>
      )
    }
  }

  const columns = [
    {
      title: 'Room Name',
      field: 'communityName',
      cellStyle: {
        minWidth: 100,
        fontSize: 12,
      },
      headerStyle: {
        minWidth: 100,
      },
    },
    {
      title: 'Occupied',
      field: 'isOccupied',
      editable: false,
      render: (rowData) => checkIfOccupied(rowData.isOccupied),
    },
  ]

  return (
    <div style={{ maxWidth: '100%' }}>
      {communityData.length > 0 && isFetchDone ? (
        <MaterialTable
          icons={tableIcons}
          title="Community"
          columns={columns}
          data={communityData}
          options={{
            search: false,
            actionsColumnIndex: -1,
            addRowPosition: 'first',
            headerStyle: {
              zIndex: 9999999,
            },
          }}
        />
      ) : (
        <p>No Data Found</p>
      )}

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default Products
