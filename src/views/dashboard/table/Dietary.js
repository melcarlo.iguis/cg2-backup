import React, { useState, useContext, forwardRef, useRef, useEffect } from 'react'
import MaterialTable from 'material-table'
import AppContext from '../../../AppContext'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
// axios api
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import AttachmentIcon from '@mui/icons-material/Attachment'

function Dietary() {
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    activeTab,
    setActiveTab,
    dailylogData,
    setDailylogData,
    setDiagnosisData,
    diagnosisData,
    dietaryData,
    setDietaryData,
  } = useContext(AppContext)

  const formatDate = (string) => {
    if (string === undefined || string === '' || string === null) {
      return string
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  //   const TitleImg = (rowData) => {
  //     return (
  //       <div>
  //         <input type="file" />
  //       </div>
  //     )
  //   }
  const columns = [
    { title: 'Dietary Preference', field: 'dietaryPreference' },
    {
      title: 'Start Date',
      field: 'startDate',
      type: 'date',
      dateSetting: { locale: 'ko-KR' },
      render: (rowData) => formatDate(rowData.startDate),
    },
    {
      title: 'DC Date',
      field: 'dcDate',
      type: 'date',
      dateSetting: { locale: 'ko-KR' },
      render: (rowData) => formatDate(rowData.dcDate),
    },
    // {
    //   title: 'Attachment',
    //   render: (rowData) => <TitleImg rowData={rowData} />,
    // },
  ]

  const AddDietaryData = (data) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const input = {
      dietaryPreference: data.dietaryPreference,
      startDate: data.startDate,
      dcDate: data.dcDate,
      ResidentId: tenantId,
    }

    // api call for adding new tenant
    api
      .post(`/dietary/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setDietaryData([...dietaryData, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // // adding history to database
        // const input2 = {
        //   title: `Added Dietary Data about ${dietaryRef} for ${tenantName}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })

        // // delay function

        // setTimeout(function () {
        //   setDialogClose(false)
        // }, 2000)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const deleteDietaryData = (data) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    api
      .put(`/dietary/${data.id}/delete`)
      .then((result) => {
        console.log(result)
        const newArr = [...dietaryData]

        newArr.forEach((item, index) => {
          if (item.id === data.id) {
            newArr.splice(index, 1)
          }
        })

        setDietaryData(newArr)
        toast.success('Deleted Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // adding history to database
        // const input2 = {
        //   title: `Deleted ${tenantName}'s dietary data about ${dietaryRef}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
      })
      .catch((err) => {})
  }

  const updateDietary = (data, id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      dietaryPreference: data.dietaryPreference,
      startDate: data.startDate,
      dcDate: data.dcDate,
    }
    // api call for adding new tenant
    api
      .put(`/dietary/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        dietaryData.forEach((item) => {
          if (item.id === id) {
            item.dietaryPreference = data.dietaryPreference
            item.startDate = data.startDate
            item.DCDate = data.dcDate
          }
        })

        setDietaryData([...dietaryData])

        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // // adding history to database
        // const input2 = {
        //   title: `Edited ${tenantName}'s dietary data about ${dietaryRef}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div style={{ maxWidth: '100%' }}>
      <MaterialTable
        icons={tableIcons}
        title="Dietary"
        style={{ zIndex: 9999999 }}
        columns={columns}
        data={dietaryData}
        editable={{
          onRowAdd: (newRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                AddDietaryData(newRow)
                resolve()
              }, 2000)
            }),
          onRowDelete: (selectedRow) =>
            new Promise((resolve, reject) => {
              console.log(selectedRow)
              setTimeout(() => {
                deleteDietaryData(selectedRow)
                resolve()
              }, 2000)
            }),
          onRowUpdate: (updatedRow, oldRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateDietary(updatedRow, oldRow.id)
                resolve()
              }, 2000)
            }),
        }}
        options={{
          search: false,
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            zIndex: 9999999,
          },
        }}
      />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default Dietary
