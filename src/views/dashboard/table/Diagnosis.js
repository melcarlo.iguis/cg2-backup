import React, { useState, useContext, forwardRef, useRef, useEffect } from 'react'
import MaterialTable from 'material-table'
import AppContext from '../../../AppContext'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
// axios api
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import AttachmentIcon from '@mui/icons-material/Attachment'

function Allergies() {
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    activeTab,
    setActiveTab,
    dailylogData,
    setDailylogData,
    setDiagnosisData,
    diagnosisData,
  } = useContext(AppContext)

  const formatDate = (string) => {
    if (string === undefined || string === '' || string === null) {
      return string
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  //   const TitleImg = (rowData) => {
  //     return (
  //       <div>
  //         <input type="file" />
  //       </div>
  //     )
  //   }
  const columns = [
    { title: 'Diagnosis', field: 'diagnosisName' },
    {
      title: 'Start Date',
      field: 'startDate',
      type: 'date',
      dateSetting: { locale: 'ko-KR' },
      render: (rowData) => formatDate(rowData.startDate),
    },
    {
      title: 'End Date',
      field: 'endDate',
      type: 'date',
      dateSetting: { locale: 'ko-KR' },
      render: (rowData) => formatDate(rowData.endDate),
    },
    // {
    //   title: 'Attachment',
    //   render: (rowData) => <TitleImg rowData={rowData} />,
    // },
  ]

  const AddDiagnosis = (data) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    console.log(data)
    const input = {
      diagnosisName: data.diagnosisName,
      startDate: data.startDate,
      endDate: data.endDate,
      ResidentId: tenantId,
    }

    // api call for adding new tenant
    api
      .post(`/diagnosis/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setDiagnosisData([...diagnosisData, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // adding history to database
        // const input2 = {
        //   title: `Added diagnosis about ${diagnosisName} of ${tenantName}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const deleteDiagnosisData = (data) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    api
      .put(`/diagnosis/${data.id}/delete`)
      .then((result) => {
        const newArr = [...diagnosisData]

        newArr.forEach((item, index) => {
          if (item.id === data.id) {
            newArr.splice(index, 1)
          }
        })

        setDiagnosisData(newArr)

        toast.success('Deleted Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // adding history to database
        const input2 = {
          title: `Deleted ${tenantName}'s diagnosis data on ${data.diagnosisName}`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {})
          .catch((err) => {
            console.error(err)
          })
      })
      .catch((err) => {
        Swal.fire({
          title: 'fail to delete',
          icon: 'error',
        })
      })
  }

  const updateDiagnosis = (data, id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      diagnosisName: data.diagnosisName,
      startDate: data.startDate,
      endDate: data.endDate,
    }

    // api call for adding new tenant
    api
      .put(`/diagnosis/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        diagnosisData.forEach((item) => {
          if (item.id === id) {
            item.diagnosisName = data.diagnosisName
            item.startDate = data.startDate
            item.endDate = data.endDate
          }
        })

        setDiagnosisData([...diagnosisData])
        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // adding history to database
        const input2 = {
          title: `Edited ${tenantName}'s diagnosis data on ${data.diagnosisName}`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {})
          .catch((err) => {
            console.error(err)
          })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div style={{ maxWidth: '100%' }}>
      <MaterialTable
        icons={tableIcons}
        title="Diagnosis"
        style={{ zIndex: 9999999 }}
        columns={columns}
        data={diagnosisData}
        editable={{
          onRowAdd: (newRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                AddDiagnosis(newRow)
                resolve()
              }, 2000)
            }),
          onRowDelete: (selectedRow) =>
            new Promise((resolve, reject) => {
              console.log(selectedRow)
              setTimeout(() => {
                deleteDiagnosisData(selectedRow)
                resolve()
              }, 2000)
            }),
          onRowUpdate: (updatedRow, oldRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateDiagnosis(updatedRow, oldRow.id)
                resolve()
              }, 2000)
            }),
        }}
        options={{
          search: false,
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            zIndex: 9999999,
          },
        }}
      />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default Allergies
