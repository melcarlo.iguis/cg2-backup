import React, { useEffect, useState, useContext, useRef } from 'react'
import { Row, Col, Form, Button, Container } from 'react-bootstrap'
import '../../../App.css'
import AddIcon from '@mui/icons-material/Add'

// global variable
import AppContext from '../../../AppContext'

import moment from 'moment'
// axios api
import api from '../../../api/api'

// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function EditObservationForm(data) {
  const {
    incidentReportList,
    setIncidentReportList,
    content,
    setContent,
    dailylogData,
    setDailylogData,
    dialogClose,
    setDialogClose,
  } = useContext(AppContext)

  console.log(dialogClose)

  const [buttonIsEnable, setButtonIsEnable] = useState(true)

  // useState for all the input fields
  const [date, setDate] = useState('')
  const [time, setTime] = useState('')
  const [note, setNote] = useState('')
  const [title, setTitle] = useState('')

  // Set all the initial value
  useEffect(() => {
    // to extract the date from datetime
    const d = new Date(data.data.date)
    console.log(d)
    const year = d.getFullYear()
    const month = d.getMonth() + 1
    const day = d.getDate()
    const dateString = '' + year + '-' + month + '-' + day

    const dateFormated = moment(dateString).format('YYYY-MM-DD')

    console.log(dateFormated)

    setDate(dateFormated)

    let hrs = d.getHours()
    let mins = d.getMinutes()

    const newMins = mins < 10 ? '0' + mins : mins

    const postTime = hrs + ':' + newMins
    setTime(postTime)

    setNote(data.data.note)
    setTitle(data.data.title)
  }, [])

  useEffect(() => {
    if (date !== '' && time !== '' && note !== '' && title !== '') {
      const timeoutId = setTimeout(() => updateObservationData(), 2000)
      return () => clearTimeout(timeoutId)
    }
  }, [date, time, note, title])

  const firstUpdate = useRef(true)

  const updateObservationData = (e) => {
    // e.preventDefault()
    const dateFormated = moment(date).format('YYYY-MM-DD')
    const dateTime = moment(dateFormated + ' ' + time)

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      date: dateTime,
      title: title,
      note: note,
    }

    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      // api call for adding new tenant
      api
        .put(`/observation/${data.data._id}/update`, input, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((result) => {
          console.log(result)

          dailylogData.forEach((item) => {
            if (item._id === data.data._id) {
              item.date = dateTime
              item.title = title
              item.note = note
            }
          })

          setDailylogData([...dailylogData])

          // adding history to database
          const input2 = {
            title: `Edited ${tenantName}'s Obervation Data`,
            tenantName: tenantName,
            tenantId: tenantId,
            userName: name,
          }
          api
            .post(`/history/create/`, input2, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {})
            .catch((err) => {
              console.error(err)
            })
        })
        .catch((err) => {
          console.log(err)
        })

      toast.success('Updated Successfully', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      })
    }
  }

  return (
    <div>
      <Form id="form">
        <Row>
          <Col md="6" sm="8" className="mx-auto colItem">
            <Form.Control
              type="date"
              name="date"
              required
              value={date}
              onChange={(e) => setDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="date">Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="time"
              name="time"
              required
              value={time}
              onChange={(e) => setTime(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="time">Time</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              placeholder=" "
              className="formItem form__input"
            />
            <label>Title</label>
          </Col>
          <Col md="10" sm="10" className="mx-auto mb-3 colItem">
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
              <Form.Label>Note</Form.Label>
              <Form.Control
                required
                as="textarea"
                rows={3}
                value={note}
                onChange={(e) => setNote(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default EditObservationForm
