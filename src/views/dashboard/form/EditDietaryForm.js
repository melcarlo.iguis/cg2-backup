import React, { useState, useEffect, useContext, useRef } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import '../../../App.css'
import AddIcon from '@mui/icons-material/Add'

// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function EditAllergyForm(data) {
  console.log(data)
  const {
    currentTab,
    allergiesList,
    setAllergiesList,
    tenantList,
    setTenantList,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    setDiagnosisData,
    diagnosisData,
    dietaryData,
    setDietaryData,
  } = useContext(AppContext)
  // State for all the input field
  const [dietaryRef, setDietaryRef] = useState('')
  const [startDate, setStartDate] = useState('')
  const [dcDate, setDcDate] = useState('')

  let startDateInput = new Date(data.data.startDate)
  let newDate = startDateInput.toISOString().substr(0, 10)
  // for Initial data from the edited data
  useEffect(() => {
    setDietaryRef(data.data.dietaryPreference)
    setStartDate(newDate)
    setDcDate(data.data.DCDate)
    checkIfDcDate()
  }, [])

  const checkIfDcDate = async () => {
    if (data.data.DCDate == '' || data.data.DCDate == undefined || data.data.DCDate == null) {
      setDcDate('')
    } else {
      let dcDateInput = new Date(data.data.DCDate)
      let newDate = dcDateInput.toISOString().substr(0, 10)
      setDcDate(newDate)
    }
  }

  const firstUpdate = useRef(true)

  const updateDietary = () => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      dietaryPreference: dietaryRef,
      startDate: startDate,
      DCDate: dcDate,
    }

    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      // api call for adding new tenant
      api
        .put(`/tenants/dietary/${tenantId}/update/${data.data._id}`, input, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((result) => {
          // updating the College student on it's state to instantly apply the changes without refreshing the page
          dietaryData.forEach((item) => {
            if (item._id === data.data._id) {
              item.dietaryPreference = dietaryRef
              item.startDate = startDate
              item.DCDate = dcDate
            }
          })

          setDietaryData([...dietaryData])

          // adding history to database
          const input2 = {
            title: `Edited ${tenantName}'s dietary data about ${dietaryRef}`,
            tenantName: tenantName,
            tenantId: tenantId,
            userName: name,
          }
          api
            .post(`/history/create/`, input2, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {})
            .catch((err) => {
              console.error(err)
            })
        })
        .catch((err) => {
          console.log(err)
        })

      toast.success('Updated Successfully', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      })
    }
  }

  useEffect(() => {
    if (dietaryRef !== '' && startDate !== '' && dcDate !== '') {
      const timeoutId = setTimeout(() => updateDietary(), 2000)
      return () => clearTimeout(timeoutId)
    }
  }, [dietaryRef, startDate, dcDate])

  return (
    <div>
      <Form id="form">
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              required
              value={dietaryRef}
              onChange={(e) => setDietaryRef(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="sportsCode">Dietary Reference</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Start Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={dcDate}
              onChange={(e) => setDcDate(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>D/C Date</label>
          </Col>
          {/*<Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>*/}
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default EditAllergyForm
