import React, { useState, useEffect, useContext } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import '../../../App.css'
import { CButton } from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function AddNewUser() {
  const { userList, setUserList, setDiagnosisData, diagnosisData, dialogClose, setDialogClose } =
    useContext(AppContext)
  // State for all the input field
  const [email, setEmail] = useState('')
  const [username, setUsername] = useState('')
  const [firstName, setFirstName] = useState('')
  const [middleName, setMiddleName] = useState('')
  const [lastName, setLastName] = useState('')
  const [password, setPassword] = useState('')
  const [userType, setUserType] = useState('')

  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')

  const registerUser = (e) => {
    e.preventDefault()
    console.log('hello')
    const input = {
      email: email,
      username: username,
      password: password,
      firstName: firstName,
      middleName: middleName,
      lastName: lastName,
    }

    // api call for adding new tenant
    api
      .post(`/users/create`, input)
      .then((result) => {
        console.log(result)
        setUserList([...userList, result.data])
        Swal.fire({
          title: 'Register Successful!',
          icon: 'success',
          text: 'Welcome',
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'Authentication failed',
          icon: 'error',
          text: 'Check Registration details',
        })
      })
  }

  return (
    <div>
      <Form id="form" onSubmit={(e) => registerUser(e)}>
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="email"
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="sportsCode">Email</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="password"
              required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="sportsCode">Password</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Username</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>First Name</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              value={middleName}
              onChange={(e) => setMiddleName(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Middle Name</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Last Name</label>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default AddNewUser
