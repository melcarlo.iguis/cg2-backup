import React, { useState, useEffect, useContext } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import '../../../App.css'
import AddIcon from '@mui/icons-material/Add'
import { CButton } from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function AddNewDiagnosis() {
  const { setDiagnosisData, diagnosisData, dialogClose, setDialogClose } = useContext(AppContext)
  // State for all the input field
  const [diagnosisName, setDiagnosisName] = useState('')
  const [startDate, setStartDate] = useState('')
  const [endDate, setEndDate] = useState('')

  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')

  const AddDiagnosis = (e) => {
    e.preventDefault()

    const input = {
      diagnosisName: diagnosisName,
      startDate: startDate,
      endDate: endDate,
      ResidentId: tenantId,
    }

    // api call for adding new tenant
    api
      .post(`/diagnosis/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setDiagnosisData([...diagnosisData, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // adding history to database
        const input2 = {
          title: `Added diagnosis about ${diagnosisName} of ${tenantName}`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {})
          .catch((err) => {
            console.error(err)
          })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div>
      <Form id="form" onSubmit={(e) => AddDiagnosis(e)}>
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              required
              value={diagnosisName}
              onChange={(e) => setDiagnosisName(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="sportsCode">Diagnosis Name</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Start Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>End Date</label>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default AddNewDiagnosis
