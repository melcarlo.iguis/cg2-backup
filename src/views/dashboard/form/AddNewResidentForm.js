import * as React from 'react'
import { useContext, useEffect, useState } from 'react'
import { Form, Col, Row } from 'react-bootstrap'
import PropTypes from 'prop-types'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import CloseIcon from '@mui/icons-material/Close'
import Typography from '@mui/material/Typography'
import Slide from '@mui/material/Slide'
import AddIcon from '@mui/icons-material/Add'
import AddBusinessIcon from '@mui/icons-material/AddBusiness'
// axios api
import api from '../../../api/api'
import '../../../App.css'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CForm,
  CFormInput,
  CFormLabel,
  CFormTextarea,
  CRow,
} from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'

function AddNewResidentForm() {
  const { tenantList, setTenantList, action, setAction, dialogClose, setDialogClose } =
    useContext(AppContext)
  const [open, setOpen] = React.useState(false)
  const [roomList, setRoomList] = useState([])

  // CODE FOR THE FORM

  const [buttonIsEnable, setButtonIsEnable] = useState(true)

  // useState for all the input fields
  const [firstName, setFirstName] = useState('')
  const [middleName, setMiddleName] = useState('')
  const [lastName, setLastName] = useState('')
  const [address, setAddress] = useState('')
  const [birthday, setBirthday] = useState('')
  const [status, setStatus] = useState('')
  const [image, setImage] = useState('')
  const [roomName, setRoomName] = useState('')
  const [careLevel, setCareLevel] = useState('')
  // useState for all the tenant's contact info
  const [contactFirstName, setContactFirstName] = useState('')
  const [contactMiddleName, setContactMiddleName] = useState('')
  const [contactLastName, setContactLastName] = useState('')
  const [contactAddress, setContactAddress] = useState('')
  const [contactNo, setContactNo] = useState('')

  // useState for handling image upload
  const [fileInputState, setFileInputState] = useState('')
  const [selectedFile, setSelectedFile] = useState('')

  const [imageFile, setImageFile] = useState('')

  // item on localStorage
  let token = localStorage.getItem('token')
  let name = localStorage.getItem('name')
  let tenantId = localStorage.getItem('tenantId')

  const handleFileInputChange = (e) => {
    const file = e.target.files[0]
    previewFile(file)
    setImageFile(file)
  }

  const previewFile = (file) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = () => {
      setImage(reader.result)
    }
  }

  // useEffect(()=>{

  // if(firstName !== ""  && lastName !== "" && address !== "" && birthday !== "" && status !== "" && image !== "" && contactFirstName !== "" && contactLastName !== "" && contactAddress !== "" && contactNo !== ""){

  //   const timeoutId = setTimeout(() => addNewResident(), 1000);
  //   return () => clearTimeout(timeoutId);

  // }

  // },[firstName , lastName, address, birthday , status, image ,contactFirstName , contactLastName , contactAddress , contactNo])
  console.log(roomName)
  const addNewResident = (e) => {
    e.preventDefault()

    console.log('hey')
    console.log(roomName)

    const formData = new FormData()

    formData.append('image', imageFile)
    formData.append('firstName', firstName)
    formData.append('middleName', middleName)
    formData.append('lastName', lastName)
    formData.append('birthday', birthday)
    formData.append('status', status)
    formData.append('address', address)
    formData.append('communityName', roomName)
    formData.append('careLevel', careLevel)

    // api call for adding new tenant
    api
      .post('/residents/add', formData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result.data.id)
        localStorage.setItem('tenantId', result.data.id)
        setTenantList([...tenantList, result.data])
        setButtonIsEnable(false)

        // code for adding contact details for tenant
        const input = {
          firstName: contactFirstName,
          middleName: contactMiddleName,
          lastName: contactLastName,
          contactNo: contactNo,
          address: contactAddress,
          ResidentId: result.data.id,
        }

        // api call for adding new tenant
        api
          .post(`/contact/add`, input, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)

            // adding history to database
            const input2 = {
              title: `${name} input a new tenant on the database`,
              tenantName:
                result.data.firstName + ' ' + result.data.middleName + ' ' + result.data.lastName,
              tenantId: result.data._id,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {
                toast.success('Added Successfully', {
                  position: 'top-right',
                  autoClose: 2000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: false,
                  draggable: true,
                  progress: undefined,
                })

                // Clear Input fields
                setImage('')
                setFirstName('')
                setMiddleName('')
                setLastName('')
                setAddress('')
                setBirthday('')
                setStatus('')
                setContactFirstName('')
                setContactMiddleName('')
                setContactLastName('')
                setContactNo('')
                setContactAddress('')

                // delay function

                setTimeout(function () {
                  setDialogClose(false)
                }, 2000)
              })
              .catch((err) => {
                console.error(err)
              })
          })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // CODE FOR THE FORM

  useEffect(() => {
    if (action == 'addResident') {
      if (dialogClose == true) {
        setOpen(true)
      } else if (dialogClose == false) {
        setOpen(false)
      }
    }
  }, [dialogClose])

  const handleClickOpen = () => {
    setAction('addResident')
    setDialogClose(true)
    setOpen(true)
  }
  const handleClose = () => {
    setAction('')
    setDialogClose(false)
    setOpen(false)
  }

  const fetchRoomList = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/community/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setRoomList(res.data)
      })
  }

  useEffect(() => {
    fetchRoomList()
  }, [])

  return (
    <div>
      {/*ADD THE FORM HERE*/}
      <div>
        <Form id="form" onSubmit={(e) => addNewResident(e)}>
          <Row>
            <Col md="12" sm="8" className="mx-auto mb-3 text-center">
              {image ? <img id="tenantPic2" src={image} alt=""></img> : null}
            </Col>
            <Col md="12" sm="8" className="mx-auto mb-3 ">
              <Col md="6" sm="8" className="mx-auto mb-3 text-center">
                <Form.Control
                  type="file"
                  // value={fileInputState}
                  onChange={handleFileInputChange}
                  placeholder="   "
                />
                <label>Select Picture</label>
              </Col>
            </Col>

            <Col md="4" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="text"
                name="fName"
                required
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label htmlFor="fName">First Name</label>
            </Col>
            <Col md="4" sm="8" className="mx-auto mb-3 colItem ">
              <Form.Control
                type="text"
                name="midName"
                value={middleName}
                onChange={(e) => setMiddleName(e.target.value)}
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label htmlFor="midName">Middle Name</label>
            </Col>
            <Col md="4" sm="8" className="mx-auto mb-3 colItem ">
              <Form.Control
                type="text"
                name="lName"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label htmlFor="lName">Last Name</label>
            </Col>

            <Col md="4" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="text"
                name="adress"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                required
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label>Address</label>
            </Col>
            <Col md="4" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="date"
                name="bDay"
                required
                value={birthday}
                onChange={(e) => setBirthday(e.target.value)}
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label>Birthday</label>
            </Col>
            <Col md="4" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="text"
                name="status"
                value={status}
                onChange={(e) => setStatus(e.target.value)}
                required
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label>Status</label>
            </Col>
            <Col md="6" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="text"
                name="careLevel"
                value={careLevel}
                onChange={(e) => setCareLevel(e.target.value)}
                required
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label>Care Level</label>
            </Col>
            <Col md="6" className="d-flex justify-content-center mt-3">
              <Autocomplete
                onChange={(event, room) => {
                  setRoomName(room.communityName)
                }}
                id="controllable-states-demo"
                options={roomList}
                getOptionLabel={(option) => `${option.communityName}`}
                sx={{ minWidth: '100%' }}
                size="small"
                renderInput={(params) => <TextField {...params} label="Community" />}
              />
            </Col>

            {/*Form for Contact info*/}
            <Col md="12">
              <div id="hr-line" className="mt-3 mb-3"></div>
              <h4 className="pb-0" id="title">
                Tenants Emergency Contact Info
              </h4>
              <em>
                <h6 className="p-0" style={{ fontSize: '12px' }}>
                  This will be the person to be contact with , when emergency happens
                </h6>
              </em>
            </Col>
            <Col md="4" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="text"
                name="fName"
                required
                value={contactFirstName}
                onChange={(e) => setContactFirstName(e.target.value)}
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label htmlFor="fName">First Name</label>
            </Col>
            <Col md="4" sm="8" className="mx-auto mb-3 colItem ">
              <Form.Control
                type="text"
                name="midName"
                value={contactMiddleName}
                onChange={(e) => setContactMiddleName(e.target.value)}
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label htmlFor="midName">Middle Name</label>
            </Col>
            <Col md="4" sm="8" className="mx-auto mb-3 colItem ">
              <Form.Control
                type="text"
                name="lName"
                value={contactLastName}
                onChange={(e) => setContactLastName(e.target.value)}
                required
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label htmlFor="lName">Last Name</label>
            </Col>

            <Col md="6" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="text"
                name="adress"
                value={contactAddress}
                onChange={(e) => setContactAddress(e.target.value)}
                required
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label>Address</label>
            </Col>
            <Col md="6" sm="8" className="mx-auto mb-3 colItem">
              <Form.Control
                type="text"
                name="adress"
                value={contactNo}
                onChange={(e) => setContactNo(e.target.value)}
                required
                placeholder=" "
                className="formItem mt-3 form__input"
              />
              <label>Contact No.</label>
            </Col>
            <Col md="12" className="d-flex justify-content-center">
              <div className="d-grid gap-2 col-6 mx-auto">
                <button className="btn btn-primary" type="submit">
                  Add Tenant
                </button>
              </div>
            </Col>

            <Col md="12" className="d-flex justify-content-center">
              <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
            </Col>
          </Row>
        </Form>
      </div>
      {/*ADD THE FORM HERE*/}
    </div>
  )
}

export default AddNewResidentForm
