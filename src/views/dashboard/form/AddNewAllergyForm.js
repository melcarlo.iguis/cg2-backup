import React, { useState, useEffect, useContext } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import '../../../App.css'
import AddIcon from '@mui/icons-material/Add'
import { CButton } from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function AddNewAllergyForm() {
  const {
    allergiesList,
    setAllergiesList,
    dialogClose,
    setDialogClose,
    dailylogData,
    setDailylogData,
  } = useContext(AppContext)
  // State for all the input field
  const [allergy, setAllergy] = useState('')
  const [allergyType, setAllergyType] = useState('')
  const [allergenType, setAllergenType] = useState('')
  const [startDate, setStartDate] = useState('')

  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')
  let userId = localStorage.getItem('userId')

  const addAllergy = (e) => {
    e.preventDefault()
    console.log('yow')

    const input = {
      allergy: allergy,
      allergyType: allergyType,
      allergenType: allergenType,
      startDate: startDate,
      ResidentId: tenantId,
    }

    // api call for adding new allergy tenant
    api
      .post(`/allergy/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        setAllergiesList([...allergiesList, result.data])

        // code for adding shiftlog summary
        const shiftSummaryLogInput = {
          activity: `${name} added ${allergy} as a new allergy data to ${tenantName}`,
          residentName: tenantName,
          userName: name,
          ResidentId: tenantId,
          UserId: userId,
        }

        console.log(shiftSummaryLogInput)

        api
          .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            // setDailylogData([...dailylogData, result.data])
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })

    // code for adding daily log
    const dailyLogInput = {
      title: `${name} added ${allergy} as a new allergy data to ${tenantName}`,
      residentName: tenantName,
      userName: name,
      ResidentId: tenantId,
    }

    api
      .post(`/dailylog/add`, dailyLogInput, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        setDailylogData([...dailylogData, result.data])
        console.log(result)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  // useEffect(() => {
  //   if (allergy !== '' && allergyType !== '' && startDate !== '') {
  //     const timeoutId = setTimeout(() => addAllergy(), 1000)
  //     return () => clearTimeout(timeoutId)
  //   }
  // }, [allergy, allergyType, startDate, allergenType])

  return (
    <div>
      <Form id="form" onSubmit={(e) => addAllergy(e)}>
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              name="sportsCode"
              required
              value={allergy}
              onChange={(e) => setAllergy(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="sportsCode">Allergy</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem ">
            <Form.Control
              type="text"
              required
              value={allergyType}
              onChange={(e) => setAllergyType(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="description">Allergy Type</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              value={allergenType}
              onChange={(e) => setAllergenType(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Allergen Type</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Start Date</label>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default AddNewAllergyForm
