import React, { useContext, useState, useEffect } from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import ListItemText from '@mui/material/ListItemText'
import ListItem from '@mui/material/ListItem'
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import CloseIcon from '@mui/icons-material/Close'
import Slide from '@mui/material/Slide'

import TenantTable from '../table/TenantTable'

import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive'
import { Icon } from '@iconify/react'

// global variable
import AppContext from '../../../AppContext'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

export default function FullScreenDialog() {
  const { content, setContent, isEmergencyOpen, setIsEmergencyOpen } = useContext(AppContext)
  const [open, setOpen] = React.useState(false)

  const handleClickOpen = () => {
    setOpen(true)
    setIsEmergencyOpen(true)
    setContent('emergency')
  }

  const handleClose = () => {
    setOpen(false)
    setIsEmergencyOpen(false)
    setContent('tenant')
  }

  return (
    <div>
      {!open ? (
        <div onClick={handleClickOpen} className="btn" id="emergency-Btn">
          <Icon id="sos-icon" icon="si-glyph:sos" />
        </div>
      ) : null}

      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar sx={{ position: 'relative' }}>
          <Toolbar id="headerEmergencyPopup">
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography sx={{ ml: 2, flex: 1 }} variant="h5" component="div">
              Tenant's Emergency Contact Information
            </Typography>
          </Toolbar>
        </AppBar>
        <TenantTable id="tenant-table" />
      </Dialog>
    </div>
  )
}
