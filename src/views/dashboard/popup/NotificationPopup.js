import React, { useContext, useEffect, useState } from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import FormControl from '@mui/material/FormControl'
import FormControlLabel from '@mui/material/FormControlLabel'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import Switch from '@mui/material/Switch'
import NotificationTable from '../table/NotificationTable'
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'

import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import AddVitalForm from '../form/AddVitalForm'
import CheckVitalsForm from '../form/CheckVitalsForm'

export default function MaxWidthDialog({ data }) {
  const [open, setOpen] = React.useState(false)
  const [fullWidth, setFullWidth] = React.useState(true)
  const [maxWidth, setMaxWidth] = React.useState('md')
  const [isFetchDone, setIsFetchDone] = useState(false)

  const [action, setAction] = useState('')
  const {
    behaviorData,
    setBehaviorData,
    setDiagnosisData,
    diagnosisData,
    notificationData,
    setNotificationData,
    notifTableData,
    setNotifTableData,
    inputValue,
    setInputValue,
  } = useContext(AppContext)

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
    setAction('')
    setInputValue('')
    localStorage.setItem('itemToModify', '')
  }

  const handleMaxWidthChange = (event) => {
    setMaxWidth(
      // @ts-expect-error autofill of arbitrary value is not handled.
      event.target.value,
    )
  }

  const handleFullWidthChange = (event) => {
    setFullWidth(event.target.checked)
  }
  let notificationId = localStorage.getItem('notificationId')
  // function for fetching notification data
  const fetchSpecificNotifData = async () => {
    let id = localStorage.getItem('notificationId')
    let token = localStorage.getItem('token')
    await api
      .get(`/notification/${id}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setNotifTableData(res.data)
        setIsFetchDone(true)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  useEffect(() => {
    fetchSpecificNotifData()
  }, [notificationId])

  console.log(notifTableData)

  const actionAvailable = [
    { action: 'Check Blood Pressure' },
    { action: 'Check Vitals' },
    { action: 'Check Temperature' },
    { action: 'Give Medicine' },
  ]

  // function for selection the action
  const handleActionSelect = (event) => {
    const data = event.target.value
    if (data == 'Check Blood Pressure') {
      setAction('action1')
    } else if (data == 'Check Vitals') {
      setAction('action2')
    } else if (data == 'Check Temperature') {
      setAction('action3')
    } else if (data == 'Give Medicine') {
      setAction('action4')
    }
  }

  console.log(notifTableData)

  return (
    <React.Fragment>
      <div id="sos-btn" variant="contained" onClick={handleClickOpen}>
        {/* <CallIcon /> */}
      </div>
      <Dialog fullWidth={fullWidth} maxWidth={maxWidth} open={open} onClose={handleClose}>
        <DialogTitle>
          <h3>{data.title}</h3>
        </DialogTitle>
        <DialogContent>
          <CheckVitalsForm />
        </DialogContent>
        {/* {notifTableData.length > 0 && isFetchDone ? (
          <>
            <DialogTitle>{notifTableData[0].title}</DialogTitle>
            <DialogContent>
              <CheckVitalsForm />
            </DialogContent>
          </>
        ) : null} */}
      </Dialog>
    </React.Fragment>
  )
}
