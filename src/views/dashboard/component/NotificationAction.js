import React, { useState, useEffect } from 'react'
import Popup from '../popup/NotificationPopup'
import api from '../../../api/api'
import {
  faBedPulse,
  faBurger,
  faClipboard,
  faFaceGrin,
  faHandDots,
  faHeartPulse,
  faPersonFalling,
  faUserDoctor,
  faBell,
  faTriangleExclamation,
  faCircleInfo,
  faPhone,
  faHospital,
  faHospitalUser,
  faCalendarCheck,
  faCalendarPlus,
  faCrutch,
  faPeopleGroup,
  faFilePdf,
  faCircleCheck,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function NotificationAction() {
  const [notificationData, setNotificationData] = useState([])

  const formatDate = (string) => {
    if (string === undefined) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' })
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const fetchNotification = async () => {
    let token = localStorage.getItem('token')
    await api
      .get(`/notification/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setNotificationData(res.data)
        // setHistoryList(res.data)
      })
  }

  const sortedNotification = notificationData.sort((a, b) => b.priorityLevel - a.priorityLevel)

  useEffect(() => {
    fetchNotification()
  }, [])
  return (
    <div id="container2" className="position-relative">
      <div>
        <strong
          style={{
            fontSize: '20px',
            marginLeft: '20px',
          }}
        >
          Notification/Action
        </strong>
        {/* Insert notification data here */}
        <div>
          {sortedNotification.length == 0 ? (
            <strong className="d-inline-block pl-2"> No Notification Data is Available</strong>
          ) : null}
          {sortedNotification.map((item, key) => (
            <div
              key={key}
              id="notification-holder"
              className={item.isCompleted == false ? '' : 'disabled-btn'}
              onClick={(e) => setNotificationId(item.id)}
            >
              <Popup data={item} />
              {item.isCompleted == false ? (
                <p className="text-right my-2">
                  {' '}
                  <FontAwesomeIcon
                    icon={faTriangleExclamation}
                    style={{ fontSize: '18px', color: '#f9b115' }}
                  />{' '}
                  This needs an attention.
                </p>
              ) : (
                <p className="text-right my-2">
                  {' '}
                  <FontAwesomeIcon
                    icon={faCircleCheck}
                    style={{ fontSize: '18px', color: '#07BC0C' }}
                  />{' '}
                  Done.
                </p>
              )}
              <p className="body-text d-inline-block"> {item.title}</p>
              {item.isCompleted == false ? (
                <p className="body-text d-inline-block pb-2"> {formatDate(item.date)}</p>
              ) : (
                <>
                  <div className="body-text d-inline-block">value: {item.value}</div>
                  <p className="body-text d-inline-block pb-2">
                    {' '}
                    Date Acoomplish:
                    {formatDate(item.dateAccomplish)}
                  </p>
                </>
              )}
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default NotificationAction
