export const LegalGuardianArr = [
  'Legal Gurdian',
  'Conservator',
  'Other Legal Oversight',
  'Durable Power of attorney/financial',
  'Family Responsible',
  'Resident Responsible for self',
]

export const CodeStatus = ['Full Code', 'DNR', 'POLST', 'Living Will/Advanced Directive', 'Other']

export const RoomPreference = ['Private', 'Shared', 'w/ Bathroom', 'w/ Shower', 'w/ Lift']

export const diagnosisLookup = [
  {
    label: 'diagnosisSample1',
  },
  {
    label: 'diagnosisSample2',
  },
  {
    label: 'diagnosisSample3',
  },
  {
    label: 'diagnosisSample4',
  },
  {
    label: 'diagnosisSample5',
  },
]

export const allergyLookup = ['Peanut', 'Egg', 'Perfume', 'Pollen', 'Seafoods']

export const diabetesControlLookup = ['Oral', 'Injection', 'Diet', 'Other', 'N/A']

export const communicableDiseasesLookup = [
  'Resident is free from communicable disease(s)',
  'Is not free from communicable disease(s)',
  'Tuberculosis',
  'Unknown at the time of assessment Score',
]

export const oxgenUsedLookup = [
  'Not Applicable: Resident does not require supplemental oxygen',
  'Independent: Resident requires supplemental oxygen and is independent with all aspects of oxygen utilization',
  'Minimal: Resident requires cueing/reminders to use supplemental oxygen as ordered by MD',
  'Moderate: Resident requires some hands on assistance with oxygen such as adjusting nasal cannula, changing tubing, ordering supplies',
  'Extensive: Resident requires assistance from a licensed nurse to adjust liters per minute, and/or monitoring of pulse oximetry, etc.',
]

export const oxygenEnablingDeviceLookup = [
  'Nasal cannula',
  'Non-Rebreather mask',
  'Portable oxygen tank',
  'Oxygen concentrator',
  'Smoking vest/blanket',
  'Pulse oximetry meter',
  'Incentive spirometer',
  'CPAP/BiPAP',
  'Humidifier',
  'Dehumidifier',
  'Air purifier',
  'Other - Resident has enabling device(s) used related to oxygenation (see Service Plan)',
  'None Apply',
]

export const medicationLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance withmedication administration',
  'Minimal: Resident requires minimal assistance (i.e. open containers or use of mediset) understands medication and treatment routine',
  'Moderate: Resident requires occasional assistance or cueing to follow medication routine or timely medication refill',
  'Extensive: Resident requires daily assistance or cueing, must be reminded to take medication and treatments; does not know medication routine',
  'Total: Resident is not able to take medication without assistance',
  'Not Applicable',
]

export const prefferedPharmacyUtilizationLookup = [
  'Resident uses community preferred pharmacy',
  'Resident uses approved non-preferred pharmacy (describe in notes)',
  'Resident uses a non-preferred pharmacy',
  'Medications are not in the community preferred packaging',
  'Over-the-counter medications not provided by community preferred pharmacy',
]
