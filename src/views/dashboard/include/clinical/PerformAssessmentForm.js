import React, { useState, useEffect, useContext, useRef } from 'react'
import TextField from '@mui/material/TextField'
import { Row, Col, Form, Button } from 'react-bootstrap'
import Divider from '@mui/material/Divider'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Autocomplete from '@mui/material/Autocomplete'
import Checkbox from '@mui/material/Checkbox'
import IconButton from '@mui/material/IconButton'
import CommentIcon from '@mui/icons-material/Comment'
import AppContext from 'src/AppContext'
import api from '../../../../api/api'
import { CButton } from '@coreui/react'
import {
  LegalGuardianArr,
  CodeStatus,
  RoomPreference,
  diagnosisLookup,
  allergyLookup,
  diabetesControlLookup,
  communicableDiseasesLookup,
  oxgenUsedLookup,
  oxygenEnablingDeviceLookup,
  medicationLevelofAssistanceLookup,
  prefferedPharmacyUtilizationLookup,
} from './ArrayOfData'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Chip from '@mui/material/Chip'
// import AppContext from '../../../../AppContext'

function PerformAssessmentForm() {
  const [value, setValue] = React.useState('1')
  const [value2, setValue2] = React.useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleChange2 = (event, newValue) => {
    setValue2(newValue)
  }

  const scrollToDiagnoses = useRef()
  const scrollToGeneral = useRef()
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    assessmentData,
    setAssessmentData,
    popupChildren,
    setPopupChildren,
    setIsPopupOpen,
  } = useContext(AppContext)
  const [tenantData, setTenantData] = useState({})
  const [assessmentDataToPerform, setAssessmentDataToPerform] = useState({})
  const [roomList, setRoomList] = useState([])

  //   input state
  const [primaryPhysician, setPrimaryPhysician] = useState('')
  const [primaryPhysicianTelNo, setPrimaryPhysicianTelNo] = useState('')
  const [primaryPhysicianAddress, setPrimaryPhysicianAddress] = useState('')
  const [mortuaryInformation, setMortuaryInformation] = useState('')

  const [productData, setProductData] = useState([])
  const [isFetchDone, setIsFetchDone] = useState(false)

  const [selectedMedicine, setSelectedMedicine] = useState([])

  // states for list box
  const [checked, setChecked] = React.useState([0])
  const [checked2, setChecked2] = React.useState([0])
  const [checked3, setChecked3] = React.useState([0])
  const [checked4, setChecked4] = React.useState([0])
  const [checked5, setChecked5] = React.useState([0])
  const [checked6, setChecked6] = React.useState([0])
  const [checked7, setChecked7] = React.useState([0])
  const [checked8, setChecked8] = React.useState([0])
  const [checked9, setChecked9] = React.useState([0])
  const [checked10, setChecked10] = React.useState([0])
  const [selectedLegalGuardian, setSelectedLegalGuardian] = React.useState([])
  const [selectedCodeStatus, setSelectedCodeStatus] = React.useState([])
  const [selectedRoom, setSelectedRoom] = React.useState([])
  const [selectedRoomPref, setSelectedRoomPref] = React.useState([])
  const [selectedDiabetesControlled, setSelectedDiabetesControlled] = React.useState([])
  const [selectedCommunicableDiseases, setSelectedCommunicableDiseases] = React.useState([])
  const [selectedOxygenUsed, setSelectedOxygenUsed] = React.useState([])
  const [selectedMedLevelAst, setSelectedMedLevelAst] = React.useState([])
  const [selectedOxygenEnablingDevice, setSelectedOxygenEnablingDevice] = React.useState([])
  const [selectedPrefPharmacy, setSelectedPrefPharmacy] = React.useState([])
  const [isSmoking, setIsSmoking] = React.useState(false)
  const [onOxygen, setOnOxygen] = React.useState(false)
  const [oxygenNote, setOxygenNote] = React.useState('')
  const [isIPPB, setIsIPBB] = React.useState(false)
  const [pbbNote, setPbbNote] = React.useState('')
  const [isColostomy, setIsColostomy] = React.useState(false)
  const [onCatheter, setOnCatheter] = React.useState(false)
  const [onBowel, setOnBowel] = React.useState(false)
  const [bowelNote, setBowelNote] = React.useState('')
  const [onContractures, setOnContractures] = React.useState(false)
  const [isDiabetes, setIsDiabetes] = React.useState(false)
  const [dialysis, setDialysis] = React.useState(false)
  const [injection, setInjection] = React.useState(false)
  const [woundBedSores, setWoundBedSores] = React.useState(false)
  const [bedridden, setBedridden] = React.useState(false)
  const [gtube, setGtube] = React.useState(false)
  const [ngTube, setNgTube] = React.useState(false)
  const [infection, setInfection] = React.useState(false)
  const [paralysis, setParalysis] = React.useState(false)
  const [historyOfFall, setHistoryOfFall] = React.useState(false)
  const [mci, setMci] = React.useState(false)
  const [conctracturesNote, setContracturesNote] = React.useState('')
  const [diabetesNote, setDiabetesNote] = React.useState('')
  const [dialysisNote, setDialysisNote] = React.useState('')
  const [injectionNote, setInjectionNote] = React.useState('')
  const [colostomyNote, setColostomyNote] = React.useState('')
  const [bedRiddenNote, setBedRiddenNote] = React.useState('')
  const [gtubeNote, setGtubeNote] = React.useState('')
  const [ngTubeNote, setNgTubeNote] = React.useState('')
  const [infectionNote, setInfectionNote] = React.useState('')
  const [paralysisNote, setParalysisNote] = React.useState('')
  const [mciNote, setMciNote] = React.useState('')
  const [historyOfFallNote, setHistoryOfFallNote] = React.useState('')
  const [woundBedSoresNote, setWoundBedSoresNote] = React.useState('')
  const [height, setHeight] = React.useState('')
  const [weight, setWeight] = React.useState('')
  const [bloodPressure, setBloodPressure] = React.useState('')
  const [pulse, setPulse] = React.useState('')
  const [respiration, setRespiration] = React.useState('')
  const [temperature, setTemperature] = React.useState('')
  const [oxygenSaturation, setOxygenSaturation] = React.useState('')

  const [isNewResidentDone, setIsNewResidentDone] = React.useState(false)
  const [isDiagnosesP1Done, setIsDiagnosesP1Done] = React.useState(false)
  const [isDiagnosesP2Done, setIsDiagnosesP2Done] = React.useState(false)
  const [isDiagnosesP5Done, setIsDiagnosesP5Done] = React.useState(false)
  const [isVitalsDone, setIsVitalsDone] = React.useState(false)

  const fixedOptions = []
  const [selectedDiagnosis, setSelectedDiagnosis] = React.useState([...fixedOptions])
  const [diagnosesNote, setDiagnosesNote] = React.useState('')

  const fixedOptions2 = []
  const [selectedAllergies, setSelectedAllergies] = React.useState([...fixedOptions2])
  const [allergyNote, setAllergyNote] = React.useState('')
  const [reasonForHopitalization, setReasonForHospitalization] = React.useState('')
  const [reasonForAdmission, setReasonForAdmission] = React.useState('')

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value)
    const newChecked = [...checked]
    const newSelectedArr = [...selectedLegalGuardian]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      newChecked.splice(currentIndex, 1)
      newSelectedArr.splice(currentIndex, 1)
    }

    setChecked(newChecked)
    setSelectedLegalGuardian(newSelectedArr)
  }

  console.log(selectedDiagnosis)

  const handleToggle2 = (value) => () => {
    const currentIndex = checked2.indexOf(value)
    const newChecked = [...checked2]
    const newSelectedArr = [...selectedCodeStatus]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      if (selectedCodeStatus.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked2(newChecked)
    setSelectedCodeStatus(newSelectedArr)
  }

  // function for fetching all the medicine

  const fetchProductData = () => {
    let token = localStorage.getItem('token')

    api
      .get('/product/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        setProductData(result.data)
        setIsFetchDone(true)
      })
  }

  console.log(productData)

  useEffect(() => {
    fetchProductData()
  }, [])

  useEffect(() => {
    // to check if all the fields are not empty
    if (
      selectedRoom.length > 0 &&
      selectedRoomPref.length > 0 &&
      selectedLegalGuardian.length > 0 &&
      primaryPhysician != '' &&
      primaryPhysicianTelNo != '' &&
      primaryPhysicianAddress != '' &&
      mortuaryInformation != ''
    ) {
      console.log('done')
      setIsNewResidentDone(true)
    } else {
      setIsNewResidentDone(false)
    }

    // diagnoses and allergy page 1
    if (
      selectedDiagnosis.length > 0 &&
      selectedAllergies.length > 0 &&
      diagnosesNote != '' &&
      allergyNote != '' &&
      reasonForHopitalization != ''
    ) {
      console.log('page one done')
      setIsDiagnosesP1Done(true)
    } else {
      setIsDiagnosesP1Done(false)
    }

    // diagnoses and allergy page 2
    if (reasonForAdmission != '' && onOxygen == false && isIPPB == false && isColostomy == false) {
      console.log('page 2 done')
      setIsDiagnosesP2Done(true)
    } else if (
      reasonForAdmission != '' &&
      onOxygen == true &&
      oxygenNote != '' &&
      isIPPB == false &&
      isColostomy == false
    ) {
      setIsDiagnosesP2Done(true)
      console.log('page 2 done')
    } else if (
      reasonForAdmission != '' &&
      onOxygen == true &&
      oxygenNote != '' &&
      isIPPB == true &&
      pbbNote != '' &&
      isColostomy == false
    ) {
      setIsDiagnosesP2Done(true)
      console.log('page 2 done')
    } else if (
      reasonForAdmission != '' &&
      onOxygen == true &&
      oxygenNote != '' &&
      isIPPB == true &&
      pbbNote != '' &&
      isColostomy == true &&
      colostomyNote != ''
    ) {
      setIsDiagnosesP2Done(true)
      console.log('page 2 done')
    } else if (
      reasonForAdmission != '' &&
      onOxygen == false &&
      isIPPB == true &&
      pbbNote != '' &&
      isColostomy == true &&
      colostomyNote != ''
    ) {
      setIsDiagnosesP2Done(true)
      console.log('page 2 done')
    } else if (
      reasonForAdmission != '' &&
      onOxygen == true &&
      oxygenNote != '' &&
      isIPPB == false &&
      isColostomy == true &&
      colostomyNote != ''
    ) {
      setIsDiagnosesP2Done(true)
      console.log('page 2 done')
    } else if (
      reasonForAdmission != '' &&
      onOxygen == false &&
      isIPPB == false &&
      isColostomy == true &&
      colostomyNote != ''
    ) {
      setIsDiagnosesP2Done(true)
      console.log('page 2 done')
    } else if (
      reasonForAdmission != '' &&
      onOxygen == false &&
      isIPPB == true &&
      pbbNote != '' &&
      isColostomy == false
    ) {
      setIsDiagnosesP2Done(true)
      console.log('page 2 done')
    } else {
      setIsDiagnosesP2Done(false)
    }

    // check if diagnoses and allergy page five is done
    if (
      selectedCommunicableDiseases.length > 0 &&
      selectedOxygenUsed.length > 0 &&
      selectedOxygenEnablingDevice.length > 0
    ) {
      setIsDiagnosesP5Done(true)
    } else {
      setIsDiagnosesP5Done(false)
    }

    // check if vitals are done
    if (
      height != '' &&
      weight != '' &&
      bloodPressure != '' &&
      pulse != '' &&
      respiration != '' &&
      temperature != '' &&
      oxygenSaturation != ''
    ) {
      setIsVitalsDone(true)
    } else {
      setIsVitalsDone(false)
    }
  }, [
    selectedRoom,
    selectedRoomPref,
    selectedLegalGuardian,
    primaryPhysician,
    primaryPhysicianTelNo,
    primaryPhysicianAddress,
    mortuaryInformation,
    selectedDiagnosis,
    selectedAllergies,
    diagnosesNote,
    allergyNote,
    reasonForHopitalization,
    reasonForAdmission,
    onOxygen,
    oxygenNote,
    isIPPB,
    pbbNote,
    isColostomy,
    colostomyNote,
    selectedCommunicableDiseases,
    selectedOxygenUsed,
    selectedOxygenEnablingDevice,
    height,
    weight,
    bloodPressure,
    pulse,
    respiration,
    temperature,
    oxygenSaturation,
  ])

  console.log(selectedOxygenUsed)

  const handleToggle3 = (value) => () => {
    const currentIndex = checked3.indexOf(value)
    const newChecked = [...checked3]
    const newSelectedArr = [...selectedRoom]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      if (selectedRoom.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked3(newChecked)
    setSelectedRoom(newSelectedArr)
  }

  console.log(selectedRoom)

  const handleToggle4 = (value) => () => {
    console.log('yoww')
    const currentIndex = checked4.indexOf(value)
    const newChecked = [...checked4]
    const newSelectedArr = [...selectedRoomPref]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      if (selectedRoomPref.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked4(newChecked)
    setSelectedRoomPref(newSelectedArr)
  }

  const handleToggle5 = (value) => () => {
    const currentIndex = checked5.indexOf(value)
    const newChecked = [...checked5]
    const newSelectedArr = [...selectedDiabetesControlled]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      if (selectedDiabetesControlled.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked5(newChecked)
    setSelectedDiabetesControlled(newSelectedArr)
  }

  const handleToggle6 = (value) => () => {
    const currentIndex = checked6.indexOf(value)
    const newChecked = [...checked6]
    const newSelectedArr = [...selectedCommunicableDiseases]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      if (selectedCommunicableDiseases.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked6(newChecked)
    setSelectedCommunicableDiseases(newSelectedArr)
  }

  const handleToggle7 = (value) => () => {
    const currentIndex = checked7.indexOf(value)
    const newChecked = [...checked7]
    const newSelectedArr = [...selectedOxygenUsed]

    if (currentIndex === -1) {
      if (selectedOxygenUsed.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
        newChecked.push(value)
        newSelectedArr.push(value)
        // toast.warn('Only one item is allow to be selected', {
        //   position: 'top-right',
        //   autoClose: 2000,
        //   hideProgressBar: false,
        //   closeOnClick: true,
        //   pauseOnHover: false,
        //   draggable: true,
        //   progress: undefined,
        // })
      } else {
        newChecked.push(value)
        newSelectedArr.push(value)
      }
    } else {
      if (selectedOxygenUsed.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked7(newChecked)
    setSelectedOxygenUsed(newSelectedArr)
  }

  console.log(selectedMedLevelAst)
  console.log(selectedPrefPharmacy)

  const handleToggle8 = (value) => () => {
    const currentIndex = checked8.indexOf(value)
    const newChecked = [...checked8]
    const newSelectedArr = [...selectedOxygenEnablingDevice]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      if (selectedOxygenEnablingDevice.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked8(newChecked)
    setSelectedOxygenEnablingDevice(newSelectedArr)
  }

  const handleToggle9 = (value) => () => {
    const currentIndex = checked9.indexOf(value)
    const newChecked = [...checked9]
    const newSelectedArr = [...selectedMedLevelAst]

    if (currentIndex === -1) {
      if (selectedMedLevelAst.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
        newChecked.push(value)
        newSelectedArr.push(value)
        // toast.warn('Only one item is allow to be selected', {
        //   position: 'top-right',
        //   autoClose: 2000,
        //   hideProgressBar: false,
        //   closeOnClick: true,
        //   pauseOnHover: false,
        //   draggable: true,
        //   progress: undefined,
        // })
      } else {
        newChecked.push(value)
        newSelectedArr.push(value)
      }
    } else {
      if (selectedMedLevelAst.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked9(newChecked)
    setSelectedMedLevelAst(newSelectedArr)
  }

  const handleToggle10 = (value) => () => {
    const currentIndex = checked10.indexOf(value)
    const newChecked = [...checked10]
    const newSelectedArr = [...selectedPrefPharmacy]

    if (currentIndex === -1) {
      newChecked.push(value)
      newSelectedArr.push(value)
    } else {
      if (selectedPrefPharmacy.length == 1) {
        newChecked.splice(0, 1)
        newSelectedArr.splice(0, 1)
      } else {
        newChecked.splice(currentIndex, 1)
        newSelectedArr.splice(currentIndex, 1)
      }
    }

    setChecked10(newChecked)
    setSelectedPrefPharmacy(newSelectedArr)
  }

  //   states
  const [room, setRoom] = useState('')
  const [careLevel, setCareLevel] = useState('')
  const [reason, setReason] = useState('')

  //api call for fetching tenant data
  let tenantId = localStorage.getItem('tenantId')
  let token = localStorage.getItem('token')
  let AssessmentCode = localStorage.getItem('AssessmentCode')

  const fetchTenant = async () => {
    const res = await api.get(`/residents/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  console.log(selectedCodeStatus)

  useEffect(() => {
    const getTenantData = async () => {
      const tenantDetails = await fetchTenant()
      if (tenantDetails) setTenantData(tenantDetails)
    }

    getTenantData()
  }, [])

  //api call for fetching assessment data
  useEffect(() => {
    fetchAssessmentData()
  }, [])

  const fetchAssessmentData = () => {
    api
      .get(`/assessments/${AssessmentCode}/fetchOne`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setAssessmentDataToPerform(res.data)
      })
  }

  //api call adding the assessment general data
  //api call adding the assessment diagnoses and allergies data
  let fullName = tenantData.firstName + ' ' + tenantData.middleName + ' ' + tenantData.lastName

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  // function for pushing the selected med to the array
  const pushSelectedMed = (data) => {
    setSelectedMedicine({ ...selectedMedicine, data })
  }

  const sampleAddData = (e) => {
    e.preventDefault()
    console.log('hello')

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    let AssessmentCode = localStorage.getItem('AssessmentCode')
    let input = {
      dateCompleted: new Date(),
    }
    api
      .put(`/assessments/${AssessmentCode}/complete`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        selectedMedicine.data.map((val) => {
          let input = {
            medLevel: selectedMedLevelAst[0],
            prefPharmacyUtilization: selectedPrefPharmacy,
            medName: val.DrugName,
            medStrength: val.Strength,
            medForm: val.Form,
            activeIngredient: val.ActiveIngredient,
            ResidentId: tenantId,
          }
          api
            .post(`/careplan/add`, input, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {
              console.log(result)
            })
            .catch((error) => {
              console.log(error)
            })
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  console.log(selectedMedicine)

  useEffect(() => {
    // set the initial value of the state
    setRoom(tenantData.communityName)
    setCareLevel(tenantData.careLevel)
    setReason(assessmentDataToPerform.type)
  }, [tenantData, assessmentDataToPerform])

  const cutDate = (string) => {
    console.log(string)
    let date = new Date(string)
    console.log(date)
    // const month = date.toLocaleString('en-us', { month: 'long' }); /* June */

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    const hour = date.getHours()
    const min = date.getMinutes()

    const newMins = min < 10 ? '0' + min : min
    var ampm = date.getHours() >= 12 ? 'PM' : 'AM'
    const newDay = day < 10 ? '0' + day : day

    let fullDate = month + '/' + newDay + '/' + year + ' - ' + hour + ':' + newMins + ' ' + ampm

    return fullDate
  }

  const AddAssessmentData = (e) => {
    e.preventDefault()
    console.log('hello')
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    let input = {
      facilityPreference: selectedRoom,
      roomPreference: selectedRoomPref,
      codeStatus: selectedCodeStatus,
      primaryPhysician: primaryPhysician,
      primaryPhysicianTelNo: primaryPhysicianTelNo,
      primaryPhysicianAddress: primaryPhysicianAddress,
      mortuaryInformation: mortuaryInformation,
      legalGuardian: selectedLegalGuardian,
      AssessmentCode: AssessmentCode,
    }

    api
      .post(`/assessmentData/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)

        // insert here
        markAssessmentAsComplete()
      })
      .catch((err) => {
        console.error(err)
      })
  }

  const markAssessmentAsComplete = () => {
    // api to mark assessment as complete
    let updateInput = {
      dateCompleted: new Date(),
      status: true,
    }
    api
      .put(`/assessments/${AssessmentCode}/complete`, updateInput, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        toast.success('Assessment Complete!', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        setPopupChildren('assessment')
      })
  }
  //   api fetch for room list
  const fetchRoomList = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/community/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setRoomList(res.data)
      })
  }

  useEffect(() => {
    fetchRoomList()
  }, [])

  console.log(selectedRoomPref)

  return (
    <div className="m-0 p-0">
      <TabContext value={value} className="m-0 p-0">
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }} className="m-0 p-0">
          <div id="sticky-tab">
            <TabList
              onChange={handleChange}
              aria-label="lab API tabs example"
              style={{ height: '20px' }}
            >
              <Tab
                icon={<CheckCircleIcon style={{ fontSize: '15px', color: 'green' }} />}
                iconPosition="end"
                label="General"
                value="1"
                style={{
                  fontSize: '10px',
                  textTransform: 'capitalize',
                  height: '20px',
                  padding: '0px',
                  margin: '0px',
                }}
              />
              <Tab
                icon={
                  isNewResidentDone ? (
                    <CheckCircleIcon style={{ fontSize: '15px', color: 'green' }} />
                  ) : null
                }
                iconPosition="end"
                label="New Resident Assessment"
                value="2"
                style={{ fontSize: '10px', textTransform: 'capitalize' }}
              />
              <Tab
                label="Diagnoses & Allergies"
                value="3"
                style={{ fontSize: '10px', textTransform: 'capitalize' }}
              />
              <Tab
                label="Vitals"
                icon={
                  isVitalsDone ? (
                    <CheckCircleIcon style={{ fontSize: '15px', color: 'green' }} />
                  ) : null
                }
                iconPosition="end"
                value="4"
                style={{ fontSize: '10px', textTransform: 'capitalize' }}
              />
              <Tab
                label="Medication"
                value="5"
                style={{ fontSize: '10px', textTransform: 'capitalize' }}
              />
            </TabList>
          </div>
        </Box>
        {/* enter general content here */}
        <TabPanel value="1">
          <div className="position-relative">
            <Divider>
              <h4>General</h4>
            </Divider>
          </div>
          <div className="d-flex justify-content-center text-center ">
            <Row>
              <Col lg={3} md={6} sm={12}>
                <TextField
                  id="standard-basic"
                  value={fullName}
                  label="Resident Name"
                  variant="standard"
                />
              </Col>
              <Col lg={3} md={6} sm={12}>
                <TextField
                  id="standard-basic"
                  value={cutBirthday(tenantData.birthday)}
                  label="Date of Birth"
                  variant="standard"
                />
              </Col>
              <Col lg={3} md={6} sm={12}>
                <TextField id="standard-basic" value={room} label="Unit" variant="standard" />
              </Col>
              <Col lg={3} md={6} sm={12}>
                <TextField
                  id="standard-basic"
                  value={careLevel}
                  label="Care Level"
                  variant="standard"
                />
              </Col>
              <Col lg={3} md={6} sm={12}>
                <TextField
                  id="standard-basic"
                  value={cutDate(assessmentDataToPerform.reviewDate)}
                  label="Assessment Date"
                  variant="standard"
                />
              </Col>
              <Col lg={3} md={6} sm={12}>
                <TextField
                  id="standard-basic"
                  value={reason}
                  label="Reason for Assessment"
                  variant="standard"
                />
              </Col>
            </Row>
          </div>
        </TabPanel>
        {/* enter new resident assessment content here */}
        <TabPanel value="2">
          <div>
            <Divider>
              <h5>Section: New Resident Assessment</h5>
            </Divider>
          </div>
          <Row>
            <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
              Facilities Preference
            </p>
            <List sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}>
              <Row>
                {roomList.map((value) => {
                  const labelId = `checkbox-list-label-${value}`

                  return (
                    <Col lg={2} md={4}>
                      <ListItem key={value.communityName} disablePadding>
                        <ListItemButton
                          role={undefined}
                          onClick={handleToggle3(value.communityName)}
                          dense
                          style={{ height: '25px', margin: '0' }}
                        >
                          <Checkbox
                            size="small"
                            color="success"
                            edge="start"
                            checked={checked3.indexOf(value.communityName) !== -1}
                            tabIndex={-1}
                            disableRipple
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                          <ListItemText
                            primary={value.communityName}
                            primaryTypographyProps={{ fontSize: '10px' }}
                          />
                        </ListItemButton>
                      </ListItem>
                    </Col>
                  )
                })}
              </Row>
            </List>
            <p style={{ fontWeight: '700', margin: '10px 0', padding: '0' }}>Room Preferences</p>
            <List sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}>
              <Row>
                {RoomPreference.map((value) => {
                  const labelId = `checkbox-list-label-${value}`

                  return (
                    <Col lg={2} md={4}>
                      <ListItem key={value} disablePadding>
                        <ListItemButton
                          role={undefined}
                          onClick={handleToggle4(value)}
                          dense
                          style={{ height: '25px', margin: '0' }}
                        >
                          <Checkbox
                            size="small"
                            edge="start"
                            color="success"
                            checked={checked4.indexOf(value) !== -1}
                            tabIndex={-1}
                            disableRipple
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                          <ListItemText
                            primary={value}
                            primaryTypographyProps={{ fontSize: '10px' }}
                          />
                        </ListItemButton>
                      </ListItem>
                    </Col>
                  )
                })}
              </Row>
            </List>
            <p style={{ fontWeight: '700', margin: '10px 0', padding: '0' }}>
              Responsibility/Legal Guardian (select all that apply)
            </p>
            <List sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}>
              <Row>
                {LegalGuardianArr.map((value) => {
                  const labelId = `checkbox-list-label-${value}`

                  return (
                    <Col lg={2} md={4}>
                      <ListItem key={value} disablePadding>
                        <ListItemButton
                          role={undefined}
                          onClick={handleToggle(value)}
                          dense
                          style={{ height: '25px', margin: '0' }}
                        >
                          <Checkbox
                            size="small"
                            edge="start"
                            color="success"
                            checked={checked.indexOf(value) !== -1}
                            tabIndex={-1}
                            disableRipple
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                          <ListItemText
                            primary={value}
                            primaryTypographyProps={{ fontSize: '10px' }}
                          />
                        </ListItemButton>
                      </ListItem>
                    </Col>
                  )
                })}
              </Row>
            </List>
            <p style={{ fontWeight: '700', padding: '0', margin: '0', marginTop: '10px' }}>
              Advanced Directives and Code Status (Select all that apply)
            </p>
            <p style={{ fontWeight: '700', padding: '0' }}>
              (Please upload physical copy to resident profile each selection)
            </p>
            <List sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}>
              <Row>
                {CodeStatus.map((value) => {
                  const labelId = `checkbox-list-label-${value}`

                  return (
                    <Col lg={2} md={4}>
                      <ListItem key={value} disablePadding>
                        <ListItemButton role={undefined} onClick={handleToggle2(value)} dense>
                          <ListItemIcon>
                            <Checkbox
                              size="small"
                              edge="start"
                              color="success"
                              checked={checked2.indexOf(value) !== -1}
                              tabIndex={-1}
                              disableRipple
                              inputProps={{ 'aria-labelledby': labelId }}
                            />
                          </ListItemIcon>
                          <ListItemText
                            id={labelId}
                            primary={value}
                            primaryTypographyProps={{ fontSize: '10px' }}
                          />
                          {/* {selectedCodeStatus.indexOf(value) > -1 ? (
                            <p>button for file upload</p>
                          ) : null} */}
                        </ListItemButton>
                      </ListItem>
                    </Col>
                  )
                })}
              </Row>
            </List>
          </Row>
          <Row>
            <Col md="4" sm="6">
              <TextField
                value={primaryPhysician}
                onChange={(e) => setPrimaryPhysician(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                label="Primary Physician"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                value={primaryPhysicianTelNo}
                InputProps={{ style: { fontSize: '10px' } }}
                onChange={(e) => setPrimaryPhysicianTelNo(e.target.value)}
                id="standard-basic"
                label="Primary Physician Telephone Number"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                value={primaryPhysicianAddress}
                InputProps={{ style: { fontSize: '10px' } }}
                onChange={(e) => setPrimaryPhysicianAddress(e.target.value)}
                id="standard-basic"
                label="Primary Physician Address"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                value={mortuaryInformation}
                InputProps={{ style: { fontSize: '10px' } }}
                onChange={(e) => setMortuaryInformation(e.target.value)}
                id="standard-basic"
                label="Mortuary Information"
                variant="standard"
              />
            </Col>
          </Row>
        </TabPanel>
        {/* enter new diagnoses and allergy content here */}
        <TabPanel style={{ margin: '0', padding: '0' }} value="3">
          <TabContext value={value2}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider', margin: '0' }}>
              <TabList
                onChange={handleChange2}
                aria-label="lab API tabs example"
                style={{ height: '20px' }}
              >
                <Tab
                  label="Page one"
                  icon={
                    isDiagnosesP1Done ? (
                      <CheckCircleIcon style={{ fontSize: '15px', color: 'green' }} />
                    ) : null
                  }
                  iconPosition="end"
                  value="1"
                  style={{ fontSize: '10px', textTransform: 'capitalize' }}
                />
                <Tab
                  label="Page two"
                  icon={
                    isDiagnosesP2Done ? (
                      <CheckCircleIcon style={{ fontSize: '15px', color: 'green' }} />
                    ) : null
                  }
                  iconPosition="end"
                  value="2"
                  style={{ fontSize: '10px', textTransform: 'capitalize' }}
                />
                <Tab
                  label="Page three"
                  value="3"
                  style={{ fontSize: '10px', textTransform: 'capitalize' }}
                />
                <Tab
                  label="Page Four"
                  value="4"
                  style={{ fontSize: '10px', textTransform: 'capitalize' }}
                />
                <Tab
                  label="Page Five"
                  icon={
                    isDiagnosesP5Done ? (
                      <CheckCircleIcon style={{ fontSize: '15px', color: 'green' }} />
                    ) : null
                  }
                  iconPosition="end"
                  value="5"
                  style={{ fontSize: '10px', textTransform: 'capitalize' }}
                />
              </TabList>
            </Box>
            {/* insert part 1 here */}
            <TabPanel value="1">
              <div ref={scrollToDiagnoses}>
                <Divider>
                  <h4>Section: Diagnoses & Allergies</h4>
                </Divider>
              </div>

              <Row>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Diagnoses
                  </p>
                </Col>
                <Col md={8}>
                  <Autocomplete
                    multiple
                    limitTags={1}
                    id="fixed-tags-demo"
                    value={selectedDiagnosis}
                    onChange={(event, newValue) => {
                      setSelectedDiagnosis([
                        ...fixedOptions,
                        ...newValue.filter((option) => fixedOptions.indexOf(option.label) === -1),
                      ])
                    }}
                    options={diagnosisLookup}
                    getOptionLabel={(option) => option.label}
                    renderTags={(tagValue, getTagProps) =>
                      tagValue.map((option, index) => (
                        <Chip
                          color="success"
                          label={option.label}
                          style={{ fontSize: '10px' }}
                          {...getTagProps({ index })}
                          disabled={fixedOptions.indexOf(option.label) !== -1}
                        />
                      ))
                    }
                    style={{ width: 450 }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Select Diagnoses"
                        placeholder="Diagnoses"
                        variant="standard"
                      />
                    )}
                  />
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Diagnoses - Other
                  </p>
                </Col>
                <Col md={8} className="mt-3">
                  <TextField
                    variant="standard"
                    value={diagnosesNote}
                    InputProps={{ style: { fontSize: '10px' } }}
                    onChange={(e) => setDiagnosesNote(e.target.value)}
                    label="Diagnoses Note"
                    multiline
                    rows={2}
                    style={{ width: 450 }}
                  />
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Allergies
                  </p>
                </Col>
                <Col md={8} className="mt-3">
                  <Autocomplete
                    multiple
                    limitTags={2}
                    id="fixed-tags-demo"
                    value={selectedAllergies}
                    onChange={(event, newValue) => {
                      setSelectedAllergies([
                        ...fixedOptions2,
                        ...newValue.filter((option) => fixedOptions2.indexOf(option) === -1),
                      ])
                    }}
                    options={allergyLookup}
                    getOptionLabel={(option) => option}
                    renderTags={(tagValue, getTagProps) =>
                      tagValue.map((option, index) => (
                        <Chip
                          color="success"
                          label={option}
                          style={{ fontSize: '10px' }}
                          {...getTagProps({ index })}
                          disabled={fixedOptions2.indexOf(option) !== -1}
                        />
                      ))
                    }
                    style={{ width: 450 }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Select Allergies"
                        placeholder="Diagnoses"
                        variant="standard"
                      />
                    )}
                  />
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Allergies - Other
                  </p>
                </Col>
                <Col md={8} className="mt-3">
                  <TextField
                    value={allergyNote}
                    InputProps={{ style: { fontSize: '10px' } }}
                    onChange={(e) => setAllergyNote(e.target.value)}
                    label="Allergies Note"
                    multiline
                    rows={2}
                    variant="standard"
                    style={{ width: 450 }}
                  />
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    {' '}
                    Reason for Hospitalization/Skilled Nursing Facility
                  </p>
                </Col>
                <Col md={8} className="mt-3">
                  <TextField
                    variant="standard"
                    value={reasonForHopitalization}
                    InputProps={{ style: { fontSize: '10px' } }}
                    onChange={(e) => setReasonForHospitalization(e.target.value)}
                    size="small"
                    style={{ width: 450, color: 'black' }}
                  />
                </Col>
              </Row>
            </TabPanel>
            {/* insert part 2 here */}
            <TabPanel value="2">
              <Row>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    {' '}
                    Reason for Admission to RCFE
                  </p>
                </Col>
                <Col md={8}>
                  <TextField
                    variant="standard"
                    label="Note"
                    InputProps={{ style: { fontSize: '10px' } }}
                    value={reasonForAdmission}
                    onChange={(e) => setReasonForAdmission(e.target.value)}
                    multiline
                    rows={3}
                    style={{ width: 450 }}
                  />
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    {' '}
                    Smoking
                  </p>
                </Col>
                <Col md={8} className="mt-3">
                  <RadioGroup
                    row
                    value={isSmoking}
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                    onChange={() => setIsSmoking((prevVal) => !prevVal)}
                  >
                    <FormControlLabel
                      value="false"
                      control={<Radio size="small" />}
                      label="Resident does not currently smoke"
                    />
                    <FormControlLabel
                      value="true"
                      control={<Radio size="small" />}
                      label="Resident currently smokes"
                    />
                  </RadioGroup>
                </Col>
                <Col md={12}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    {' '}
                    Does the Applicant use/has the following? Please Document if they need
                    asssistance with.
                  </p>
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Oxygen
                  </p>
                </Col>
                <Col md={8} className="mt-3 d-flex">
                  <Col md={3}>
                    {' '}
                    <RadioGroup
                      row
                      value={onOxygen}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setOnOxygen((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      value={onOxygen ? oxygenNote : ''}
                      onChange={(e) => setOxygenNote(e.target.value)}
                      size="small"
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                      label="Note"
                    />
                  </Col>
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Intermittent Positive Pressure Breathing(IPPB)
                  </p>
                </Col>
                <Col md={8} className="mt-3 d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={isIPPB}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setIsIPBB((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      value={pbbNote}
                      onChange={(e) => setPbbNote(e.target.value)}
                      size="small"
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                      label="Note"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4} className="mt-3">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Colostomy/Ileostomy
                  </p>
                </Col>
                <Col md={8} className="mt-3 d-flex">
                  {' '}
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={isColostomy}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setIsColostomy((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      value={colostomyNote}
                      onChange={(e) => setColostomyNote(e.target.value)}
                      size="small"
                      label="Note"
                      variant="standard"
                      style={{ width: '270px', color: 'black' }}
                    />
                  </Col>{' '}
                </Col>
              </Row>
            </TabPanel>
            {/* insert part 3 here */}
            <TabPanel value="3">
              <Row>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Enema/Suppository/Fecal Impaction Removal
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={isColostomy}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setIsColostomy((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Catheter
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={onCatheter}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setOnCatheter((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Bowel and/or Bladder Incontinence
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={onBowel}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setOnBowel((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={bowelNote}
                      onChange={(e) => setBowelNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Contractures (If yes, please describe)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={onContractures}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setOnContractures((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={conctracturesNote}
                      onChange={(e) => setContracturesNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Diabetes
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={isDiabetes}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setIsDiabetes((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={diabetesNote}
                      onChange={(e) => setDiabetesNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={12}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    How is diabetes controlled? (Select all that apply)
                  </p>
                  <List
                    sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}
                  >
                    <Row>
                      {diabetesControlLookup.map((value) => {
                        const labelId = `checkbox-list-label-${value}`

                        return (
                          <Col lg={2} md={4}>
                            <ListItem key={value} disablePadding>
                              <ListItemButton
                                role={undefined}
                                onClick={handleToggle5(value)}
                                dense
                                style={{ height: '25px', margin: '0' }}
                              >
                                <Checkbox
                                  size="small"
                                  color="success"
                                  edge="start"
                                  checked={checked5.indexOf(value) !== -1}
                                  tabIndex={-1}
                                  disableRipple
                                  inputProps={{ 'aria-labelledby': labelId }}
                                />
                                <ListItemText
                                  primary={value}
                                  primaryTypographyProps={{ fontSize: '10px' }}
                                />
                              </ListItemButton>
                            </ListItem>
                          </Col>
                        )
                      })}
                    </Row>
                  </List>
                </Col>
                <Col md={4} className="mt-2">
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Dialysis (If yes, explain)
                  </p>
                </Col>
                <Col md={8} className="d-flex mt-2">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={dialysis}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setDialysis((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={dialysisNote}
                      onChange={(e) => setDialysisNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Injections (If yes, please describe)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={injection}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setInjection((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={injectionNote}
                      onChange={(e) => setInjectionNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
              </Row>
            </TabPanel>
            <TabPanel value="4">
              <Row>
                {' '}
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Wound/Bed Sores (If yes, Describe stage and details.)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={woundBedSores}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setWoundBedSores((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={woundBedSoresNote}
                      onChange={(e) => setWoundBedSoresNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Bedridden (If yes, please describe)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={bedridden}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setBedridden((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={bedRiddenNote}
                      onChange={(e) => setBedRiddenNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    G-Tube/Feeding Tube (If yes, please describe)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={gtube}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setGtube((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={gtubeNote}
                      onChange={(e) => setGtubeNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Naso-Gastric Tube (NG Tube) (If yes, Please describe)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={ngTube}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setNgTube((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={ngTubeNote}
                      onChange={(e) => setNgTubeNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Infections (If yes, please describe)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={infection}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setInfection((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={infectionNote}
                      onChange={(e) => setInfectionNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Paralysis (If yes, please describe)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={paralysis}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setParalysis((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={paralysisNote}
                      onChange={(e) => setParalysisNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    History of Falls (Explain)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={historyOfFall}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setHistoryOfFall((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={historyOfFallNote}
                      onChange={(e) => setHistoryOfFallNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
                <Col md={4}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Dementia, Mild Cognitive Impairment (MCI)
                  </p>
                </Col>
                <Col md={8} className="d-flex">
                  <Col md={3}>
                    <RadioGroup
                      row
                      value={mci}
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                      onChange={() => setMci((prevVal) => !prevVal)}
                    >
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="true"
                        control={<Radio size="small" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        style={{ fontSize: '15px' }}
                        value="false"
                        control={<Radio size="small" />}
                        label="No"
                      />
                    </RadioGroup>
                  </Col>
                  <Col md={9}>
                    <TextField
                      size="small"
                      value={mciNote}
                      onChange={(e) => setMciNote(e.target.value)}
                      style={{ width: '270px', color: 'black' }}
                      variant="standard"
                    />
                  </Col>{' '}
                </Col>
              </Row>
            </TabPanel>
            <TabPanel value="5">
              <Row>
                <Col md={12}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Communicable Diseases (Select all that apply)
                  </p>
                  <List
                    sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}
                  >
                    <Row>
                      {communicableDiseasesLookup.map((value) => {
                        const labelId = `checkbox-list-label-${value}`

                        return (
                          <Col lg={4} md={6}>
                            <ListItem key={value} disablePadding>
                              <ListItemButton
                                role={undefined}
                                onClick={handleToggle6(value)}
                                dense
                                style={{ height: '25px', margin: '0' }}
                              >
                                <Checkbox
                                  size="small"
                                  color="success"
                                  edge="start"
                                  checked={checked6.indexOf(value) !== -1}
                                  tabIndex={-1}
                                  disableRipple
                                  inputProps={{ 'aria-labelledby': labelId }}
                                />
                                <ListItemText
                                  primary={value}
                                  primaryTypographyProps={{ fontSize: '10px' }}
                                />
                              </ListItemButton>
                            </ListItem>
                          </Col>
                        )
                      })}
                    </Row>
                  </List>
                </Col>
                <Col md={12}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Oxygen Use (Select one)
                  </p>
                  <List
                    sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}
                  >
                    <Row>
                      {oxgenUsedLookup.map((value) => {
                        const labelId = `checkbox-list-label-${value}`

                        return (
                          <Col lg={6} md={12}>
                            <ListItem key={value} disablePadding>
                              <ListItemButton
                                role={undefined}
                                onClick={handleToggle7(value)}
                                dense
                                style={{ height: '35px', margin: '0' }}
                              >
                                <Checkbox
                                  size="small"
                                  color="success"
                                  edge="start"
                                  checked={checked7.indexOf(value) !== -1}
                                  tabIndex={-1}
                                  disableRipple
                                  inputProps={{ 'aria-labelledby': labelId }}
                                />
                                <ListItemText
                                  primary={value}
                                  primaryTypographyProps={{ fontSize: '10px' }}
                                />
                              </ListItemButton>
                            </ListItem>
                          </Col>
                        )
                      })}
                    </Row>
                  </List>
                </Col>
                <Col md={12}>
                  <p
                    style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}
                  >
                    Oxygen Enabling Devices and Methods (Select all that apply)
                  </p>
                  <List
                    sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}
                  >
                    <Row>
                      {oxygenEnablingDeviceLookup.map((value) => {
                        const labelId = `checkbox-list-label-${value}`

                        return (
                          <Col lg={3} md={6}>
                            <ListItem key={value} disablePadding>
                              <ListItemButton
                                role={undefined}
                                onClick={handleToggle8(value)}
                                dense
                                style={{ height: '25px', margin: '0' }}
                              >
                                <Checkbox
                                  size="small"
                                  color="success"
                                  edge="start"
                                  checked={checked8.indexOf(value) !== -1}
                                  tabIndex={-1}
                                  disableRipple
                                  inputProps={{ 'aria-labelledby': labelId }}
                                />
                                <ListItemText
                                  primary={value}
                                  primaryTypographyProps={{ fontSize: '10px' }}
                                />
                              </ListItemButton>
                            </ListItem>
                          </Col>
                        )
                      })}
                    </Row>
                  </List>
                </Col>
              </Row>
            </TabPanel>
          </TabContext>
        </TabPanel>
        <TabPanel value="4">
          {' '}
          <div>
            <Divider>
              <h5>Section: Vitals During Visit/Assessment</h5>
            </Divider>
          </div>
          <Row>
            <Col md={3}>
              {' '}
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Height
              </p>
            </Col>
            <Col md={3}>
              {' '}
              <TextField
                value={height}
                onChange={(e) => setHeight(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                variant="standard"
              />
            </Col>
            <Col md={3}>
              {' '}
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Weight
              </p>
            </Col>
            <Col md={3}>
              {' '}
              <TextField
                value={weight}
                onChange={(e) => setWeight(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                variant="standard"
              />
            </Col>
            <Col md={3}>
              {' '}
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Blood Pressure
              </p>
            </Col>
            <Col md={3}>
              {' '}
              <TextField
                value={bloodPressure}
                onChange={(e) => setBloodPressure(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                variant="standard"
              />
            </Col>
            <Col md={3}>
              {' '}
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Pulse
              </p>
            </Col>
            <Col md={3}>
              {' '}
              <TextField
                value={pulse}
                onChange={(e) => setPulse(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                variant="standard"
              />
            </Col>
            <Col md={3}>
              {' '}
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Respiration
              </p>
            </Col>
            <Col md={3}>
              {' '}
              <TextField
                value={respiration}
                onChange={(e) => setRespiration(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                variant="standard"
              />
            </Col>
            <Col md={3}>
              {' '}
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Temperature
              </p>
            </Col>
            <Col md={3}>
              {' '}
              <TextField
                value={temperature}
                onChange={(e) => setTemperature(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                variant="standard"
              />
            </Col>
            <Col md={3}>
              {' '}
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Oxygen Saturation
              </p>
            </Col>
            <Col md={3}>
              {' '}
              <TextField
                value={oxygenSaturation}
                onChange={(e) => setOxygenSaturation(e.target.value)}
                fullWidth
                InputProps={{ style: { fontSize: '10px' } }}
                id="standard-basic"
                variant="standard"
              />
            </Col>
          </Row>
        </TabPanel>
        <TabPanel value="5">
          <Row>
            <Col md={12}>
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Medication Level of Assistance (Select one)
              </p>
              <List sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}>
                <Row>
                  {medicationLevelofAssistanceLookup.map((value) => {
                    const labelId = `checkbox-list-label-${value}`

                    return (
                      <Col lg={6} md={12}>
                        <ListItem key={value} disablePadding>
                          <ListItemButton
                            role={undefined}
                            onClick={handleToggle9(value)}
                            dense
                            style={{ height: '35px', margin: '0' }}
                          >
                            <Checkbox
                              size="small"
                              color="success"
                              edge="start"
                              checked={checked9.indexOf(value) !== -1}
                              tabIndex={-1}
                              disableRipple
                              inputProps={{ 'aria-labelledby': labelId }}
                            />
                            <ListItemText
                              primary={value}
                              primaryTypographyProps={{ fontSize: '10px' }}
                            />
                          </ListItemButton>
                        </ListItem>
                      </Col>
                    )
                  })}
                </Row>
              </List>
            </Col>
            <Col md={12}>
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Preferred Pharmacy Utilization (Select all that apply)
              </p>
              <List sx={{ width: '100%', bgcolor: 'background.paper', margin: '0', padding: '0' }}>
                <Row>
                  {prefferedPharmacyUtilizationLookup.map((value) => {
                    const labelId = `checkbox-list-label-${value}`

                    return (
                      <Col lg={6} md={12}>
                        <ListItem key={value} disablePadding>
                          <ListItemButton
                            role={undefined}
                            onClick={handleToggle10(value)}
                            dense
                            style={{ height: '35px', margin: '0' }}
                          >
                            <Checkbox
                              size="small"
                              color="success"
                              edge="start"
                              checked={checked10.indexOf(value) !== -1}
                              tabIndex={-1}
                              disableRipple
                              inputProps={{ 'aria-labelledby': labelId }}
                            />
                            <ListItemText
                              primary={value}
                              primaryTypographyProps={{ fontSize: '10px' }}
                            />
                          </ListItemButton>
                        </ListItem>
                      </Col>
                    )
                  })}
                </Row>
              </List>
            </Col>
            <Col md={4}>
              <p style={{ fontWeight: '700', margin: ' 0', padding: '0', marginBottom: '10px' }}>
                Medications (List all medications Regular and PRN)
              </p>
            </Col>
            <Col md={8}>
              <Autocomplete
                multiple
                limitTags={1}
                id="fixed-tags-demo"
                value={selectedDiagnosis}
                onChange={(event, newValue) => {
                  setSelectedDiagnosis([
                    ...fixedOptions,
                    ...newValue.filter((option) => fixedOptions.indexOf(option.DrugName) === -1),
                  ])
                  pushSelectedMed(newValue)
                }}
                options={productData}
                getOptionLabel={(option) =>
                  option.DrugName + ' ' + '(' + option.Strength.slice(0, 20) + ')'
                }
                renderTags={(tagValue, getTagProps) =>
                  tagValue.map((option, index) => (
                    <Chip
                      color="success"
                      label={option.DrugName}
                      style={{ fontSize: '10px' }}
                      {...getTagProps({ index })}
                      disabled={fixedOptions.indexOf(option) !== -1}
                    />
                  ))
                }
                style={{ width: 450 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Medication"
                    placeholder="Medicine"
                    variant="standard"
                  />
                )}
              />
            </Col>
            <div className="mx-auto mt-5 d-flex justify-content-center">
              <CButton onClick={(e) => sampleAddData(e)} id="submit-btn">
                Submit Assessment
              </CButton>
            </div>
          </Row>
        </TabPanel>
      </TabContext>
    </div>
  )
}

export default PerformAssessmentForm
