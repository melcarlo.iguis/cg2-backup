import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table, Container } from 'react-bootstrap'
import AppContext from '../../../../AppContext'

import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'
import ReportIcon from '@mui/icons-material/Report'
import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import TaskAltIcon from '@mui/icons-material/TaskAlt'

// axios api
import api from '../../../../api/api'
import AddNewBehaviorForm from '../../form/AddNewBehaviorForm'

// Icons
import SearchIcon from '@mui/icons-material/Search'
import InputBase from '@material-ui/core/InputBase'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import AttachmentIcon from '@mui/icons-material/Attachment'
import EditIcon from '@mui/icons-material/Edit'
import Popup from '../../popup/Popup'
import BehaviorTable from '../../table/BehaviorTable'
import { FALSE } from 'sass'
function CarePlan() {
  const [careplanData, setCareplanData] = useState([])
  const [careplanDataComplete, setCareplanDataComplete] = useState([])

  const formatDate = (string) => {
    if (string === undefined || string === '') {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate() + 1
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const fetchCareplanData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/careplan/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        const newFilteredArr = result.data.filter((item) => {
          return item.status == true
        })
        setCareplanData(newFilteredArr)
        const completedCareplan = result.data.filter((item) => {
          return item.status == false
        })
        setCareplanDataComplete(completedCareplan)
      })
  }

  useEffect(() => {
    fetchCareplanData()
  }, [])

  const markAsComplete = (e, id) => {
    e.preventDefault()
    console.log(id)
    let token = localStorage.getItem('token')
    api
      .put(`/careplan/${id}/complete`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const sortedArray = careplanData.sort((a, b) => b.priorityLevel - a.priorityLevel)
  const sortedArrayComplete = careplanDataComplete.sort((a, b) => b.priorityLevel - a.priorityLevel)

  console.log(sortedArray)
  const [value, setValue] = React.useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <TabContext value={value}>
      <Box>
        <TabList onChange={handleChange}>
          <Tab label="Unfinish" value="1" />
          <Tab label="Completed" value="2" className="mx-2" />
        </TabList>
      </Box>
      <TabPanel value="1">
        <Container fluid>
          <Row>
            {sortedArray.map((item, key) => (
              <Col md={6} key={key}>
                <div className="careplan-holder">
                  <Col md={12}>
                    <div className="title d-flex">
                      {item.medName} (<p>{item.activeIngredient}</p>) {item.medStrength}
                    </div>
                    <ReportIcon
                      className={
                        item.priorityLevel > 10
                          ? 'high'
                          : item.priorityLevel < 10 && item.priorityLevel > 5
                          ? 'med'
                          : 'low'
                      }
                    />
                    <p className="form d-flex">
                      <p className="m-0 p-0" style={{ fontWeight: 700 }}>
                        Form:
                      </p>{' '}
                      {item.medForm}
                    </p>
                    <p className="form">
                      <p className="m-0 p-0" style={{ fontWeight: 700 }}>
                        Medication Level of Assistance:
                      </p>
                      {item.medLevel}
                    </p>
                    <div className="mark-as-done" onClick={(e) => markAsComplete(e, item.id)}>
                      <TaskAltIcon className="ico" />
                    </div>
                  </Col>
                </div>
              </Col>
            ))}
          </Row>
        </Container>
      </TabPanel>
      <TabPanel value="2">
        <Container fluid>
          <Row>
            {sortedArrayComplete.map((item, key) => (
              <Col md={6} key={key}>
                <div className="careplan-holder">
                  <Col md={12}>
                    <div className="title d-flex">
                      {item.medName} (<p>{item.activeIngredient}</p>) {item.medStrength}
                    </div>
                    <p className="form d-flex">
                      <p className="m-0 p-0" style={{ fontWeight: 700 }}>
                        Form:
                      </p>{' '}
                      {item.medForm}
                    </p>
                    <p className="form">
                      <p className="m-0 p-0" style={{ fontWeight: 700 }}>
                        Medication Level of Assistance:
                      </p>
                      {item.medLevel}
                    </p>
                    <p className="form ml-2 pl-2 align-right" style={{ fontWeight: 700 }}>
                      Date Accomplish: {formatDate(item.updatedAt)}
                    </p>
                  </Col>
                </div>
              </Col>
            ))}
          </Row>
        </Container>
      </TabPanel>
    </TabContext>
  )
}

export default CarePlan
