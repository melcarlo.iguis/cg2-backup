import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../../AppContext'

import { DataGrid } from '@mui/x-data-grid'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import Swal from 'sweetalert2'
// axios api
import api from '../../../../api/api'
// MUI
import InputBase from '@material-ui/core/InputBase'

import EditPopup from '../../popup/EditPopup'
import EditDietaryForm from '../../form/EditDietaryForm'
// icons
import SearchIcon from '@mui/icons-material/Search'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import Popup from '../../popup/Popup'
// import EditPopup from '../popup/EditPopup'
import AddNewDietary from '../../form/AddNewDietary'

function Dietary() {
  // state for search word
  const [wordEntered, setWordEntered] = useState('')
  const [isFetchDone, setIsFetchDone] = useState(true)

  // global variables
  const { dietaryData, setDietaryData } = useContext(AppContext)
  let tenantId = localStorage.getItem('tenantId')

  const fetchDietaryData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    console.log('hello')
    api
      .get(`/dietary/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setDietaryData(res.data)
        setIsFetchDone(true)
      })
  }

  useEffect(() => {
    fetchDietaryData()
  }, [tenantId])

  // function for search input
  // const handleFilter = async (event) => {
  //   const searchWord = event.target.value
  //   setWordEntered(searchWord)

  //   let token = localStorage.getItem('token')
  //   let tenantId = localStorage.getItem('tenantId')

  //   await api
  //     .get(`/tenants/dietary/${tenantId}/fetch`, {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     })
  //     .then((res) => {
  //       setDietaryData(res.data)

  //       const newFilter = res.data.filter((value) => {
  //         return value.dietaryPreference.toLowerCase().includes(searchWord.toLowerCase())
  //       })
  //       setDietaryData(newFilter)
  //     })
  // }

  const deleteDietaryData = (e, id, dietaryRef) => {
    e.preventDefault()

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .put(`/dietary/${id}/delete`)
          .then((result) => {
            const newArr = [...dietaryData]

            newArr.forEach((item, index) => {
              if (item.id === id) {
                newArr.splice(index, 1)
              }
            })

            setDietaryData(newArr)
            Swal.fire('Deleted!', 'The Data has been deleted.', 'success')

            // adding history to database
            const input2 = {
              title: `Deleted ${tenantName}'s dietary data about ${dietaryRef}`,
              tenantName: tenantName,
              tenantId: tenantId,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {})
              .catch((err) => {
                console.error(err)
              })
          })
          .catch((err) => {
            Swal.fire({
              title: 'fail to delete',
              icon: 'error',
            })
          })
      }
    })
  }

  const formatDate = (string) => {
    if (string === undefined || string === '' || string == null) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const columns = [
    {
      field: 'dietaryPreference',
      headerName: 'Dietary Preference',
      minWidth: 220,
      // sortable: false,
      // valueGetter: (params) => {
      //   return cutDate(params.row.date)
      // },
    },
    {
      field: 'startDate',
      headerName: 'Start Date',
      flex: 1,
      minWidth: 480,
      sortable: false,
      valueGetter: (params) => {
        return formatDate(params.row.startDate)
      },
    },
    {
      field: 'dcDate',
      headerName: 'D/C Date',
      flex: 1,
      minWidth: 80,
      valueGetter: (params) => {
        return formatDate(params.row.DCDate)
      },
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>
            {' '}
            <EditDietaryForm data={params.row} />
          </EditPopup>
          <div
            id="delete-btn"
            className="btn d-flex"
            onClick={(e) => deleteDietaryData(e, params.row.id, params.row.diagnosisName)}
          >
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  return (
    <div id="container" className="mt-3">
      <div className="d-flex">
        <h1 className="mx-auto mt-2">Dietary</h1>
      </div>
      <div
        style={{
          marginLeft: '25px',
        }}
      >
        <Popup>
          <AddNewDietary />
        </Popup>
      </div>
      <Row>
        {/* <Col className="mb-3" md="12">
          <div id="search-bar-container">
            <InputBase
              id="searchBar"
              placeholder="Search…"
              value={wordEntered}
              onChange={handleFilter}
              inputProps={{ 'aria-label': 'search' }}
              endAdornment={<SearchIcon style={{ fontSize: 25 }} className="pr-3" />}
            />
          </div>
        </Col> */}
        <Col md="12" className="mx-auto"></Col>
        <div id="datagrid-container" className="mx-auto">
          <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-3">
            {dietaryData.length > 0 && isFetchDone == true ? (
              <DataGrid
                getRowId={(row) => row.id}
                rows={dietaryData}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
              />
            ) : (
              <p>No Data Available</p>
            )}
          </Col>
        </div>
      </Row>
    </div>
  )
}

export default Dietary
