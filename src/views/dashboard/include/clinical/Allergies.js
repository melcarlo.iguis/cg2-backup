import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../../AppContext'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'
import { Grid, Box, CardActionArea } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
// axios api
import api from '../../../../api/api'

import Popup from '../../popup/Popup'
import AddNewAllergyForm from '../../form/AddNewAllergyForm'
import Swal from 'sweetalert2'
import EditPopup from '../../popup/EditPopup'
import EditAllergyForm from '../../form/EditAllergyForm'

// Icons
import SearchIcon from '@mui/icons-material/Search'
import InputBase from '@material-ui/core/InputBase'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import AttachmentIcon from '@mui/icons-material/Attachment'
import EditIcon from '@mui/icons-material/Edit'

function Allergies({ activeTabVal }) {
  // global variables
  const { allergiesList, setAllergiesList, currentTab, setCurrentTab } = useContext(AppContext)
  // state for search word
  const [wordEntered, setWordEntered] = useState('')
  const [allergyTypeInput, setAllergyTypeInput] = useState('')
  const [isFetchDone, setIsFetchDone] = useState(false)
  let token = localStorage.getItem('token')

  useEffect(() => {
    setAllergyTypeInput('all')
    setCurrentTab(parseInt(activeTabVal))
  }, [])

  // for filtering the course data based on department named
  useEffect(() => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    api
      .get(`/allergy/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setAllergiesList(res.data)
        console.log(res)
        if (allergyTypeInput !== 'all') {
          const newFilter = res.data.filter((value) => {
            return value.allergyType.toLowerCase().includes(allergyTypeInput.toLowerCase())
          })

          setAllergiesList(newFilter)
          console.log(newFilter)
        }
      })
  }, [allergyTypeInput])

  // code for fetching allergy
  useEffect(() => {
    fetchAllergy()
  }, [])

  const fetchAllergy = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    api
      .get(`/allergy/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setAllergiesList(res.data)
        setIsFetchDone(true)
      })
  }

  // function for search input
  const handleFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get(`/allergy/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setAllergiesList(res.data)

        const newFilter = res.data.filter((value) => {
          return value.allergy.toLowerCase().includes(searchWord.toLowerCase())
        })
        setAllergiesList(newFilter)
      })
  }

  const deleteAllergy = (e, id, allergy) => {
    e.preventDefault()

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    console.log(id)

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .put(`/allergy/${id}/delete`)
          .then((result) => {
            console.log(result)

            const newArr = [...allergiesList]
            newArr.forEach((item, index) => {
              if (item.id === id) {
                newArr.splice(index, 1)
              }
            })

            setAllergiesList(newArr)
            Swal.fire('Deleted!', 'The Data has been deleted.', 'success')

            // adding history to database
            const input2 = {
              title: `Deleted ${tenantName}'s allergy data on ${allergy}`,
              tenantName: tenantName,
              tenantId: tenantId,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {})
              .catch((err) => {
                console.error(err)
              })
          })
          .catch((err) => {
            Swal.fire({
              title: 'fail to delete',
              icon: 'error',
            })
          })
      }
    })
  }

  const formatDate = (string) => {
    if (string === undefined || string === '') {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const columns = [
    {
      field: 'allergy',
      headerName: 'Allergy',
      minWidth: 120,
      flex: 0.5,
    },
    {
      field: 'allergyType',
      headerName: 'Allergy Type',
      width: 120,
    },

    {
      field: 'allergenType',
      headerName: 'Allergen Type',
      flex: 1,
      minWidth: 80,
    },
    {
      field: 'startDate',
      headerName: 'Start Date',
      minWidth: 150,
      sortable: false,
      flex: 0.5,
      valueGetter: (params) => {
        return formatDate(params.row.startDate)
      },
    },
    {
      field: 'endDate',
      headerName: 'End Date',
      minWidth: 220,
      sortable: false,
      valueGetter: (params) => {
        return formatDate(params.row.endDate)
      },
    },
    {
      field: 'reaction',
      headerName: 'Reaction',
      flex: 1,
      minWidth: 200,
    },
    {
      field: 'attachment',
      headerName: 'Add Attachment',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div id="edit-Btn" className="btn">
          Add
          <AttachmentIcon />
        </div>
      ),
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>
            <EditAllergyForm data={params.row} />
          </EditPopup>
          <div
            id="delete-btn"
            className="btn d-flex"
            onClick={(e) => deleteAllergy(e, params.row.id, params.row.allergy)}
          >
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  console.log(allergiesList)

  return (
    <div id="container" className="mt-3">
      <div className="title-holder d-flex">
        <h1 className="mx-auto mt-2">Allergies</h1>
      </div>
      <Row>
        {/*start*/}
        <Grid container rowSpacing={4} columnSpacing={{ xs: 1, sm: 1, md: 1 }}>
          <Grid
            item
            md={9}
            sm={10}
            direction="column"
            className="ml-5"
            style={{
              marginLeft: '25px',
            }}
          >
            <Popup className="ml-5">
              <AddNewAllergyForm />
            </Popup>
          </Grid>

          <Grid
            item
            md={2}
            sm={10}
            direction="column"
            className="ml-5"
            style={{
              marginLeft: '25px',
            }}
          >
            <Box id="drop-Drown" className="pr-5" sx={{ minWidth: 120 }}>
              <FormControl fullWidth>
                <InputLabel variant="standard" htmlFor="uncontrolled-native">
                  Allergy Type
                </InputLabel>
                <NativeSelect
                  value={allergyTypeInput}
                  onChange={(e) => setAllergyTypeInput(e.target.value)}
                >
                  ><option value="Food">Food</option>
                  <option value="Environment">Environment</option>
                  <option value="Medicine">Medicine</option>
                  <option value="all">All</option>
                </NativeSelect>
              </FormControl>
            </Box>

            {/*<InputBase
							  placeholder="Search…"
							  value={wordEntered}
		           			  onChange={handleFilter}
							  inputProps={{ 'aria-label': 'search' }}
							  className="pl-4"

							  endAdornment={<SearchIcon style={{ fontSize: 50 }} className="pl-3"/>}
							
							/>*/}
          </Grid>
        </Grid>

        {/*end*/}
        <div className="mt-3 mx-auto" id="datagrid-container">
          <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-5">
            {allergiesList.length > 0 && isFetchDone == true ? (
              <DataGrid
                getRowId={(row) => row.id}
                rows={allergiesList}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
              />
            ) : (
              <p>No Available Data</p>
            )}
          </Col>
        </div>
      </Row>
    </div>
  )
}

export default Allergies
