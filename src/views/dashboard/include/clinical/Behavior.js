import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../../AppContext'

import Box from '@mui/material/Box'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'

import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'

// axios api
import api from '../../../../api/api'
import AddNewBehaviorForm from '../../form/AddNewBehaviorForm'

// Icons
import SearchIcon from '@mui/icons-material/Search'
import InputBase from '@material-ui/core/InputBase'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import AttachmentIcon from '@mui/icons-material/Attachment'
import EditIcon from '@mui/icons-material/Edit'
import Popup from '../../popup/Popup'
import BehaviorTable from '../../table/BehaviorTable'

function Behavior() {
  const [expanded, setExpanded] = React.useState(false)

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false)
  }
  // global variables
  const { behaviorData, setBehaviorData, setDiagnosisData, diagnosisData } = useContext(AppContext)

  const [noOfActive, setNoOfActive] = useState(0)
  const [noOfInactive, setNoOfInactive] = useState(0)

  let tenantId = localStorage.getItem('tenantId')

  useEffect(() => {
    fetchBehaviorData()
  }, [behaviorData])

  const fetchBehaviorData = () => {
    let token = localStorage.getItem('token')
    api
      .get(`/behavior/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const filterInactive = res.data.filter((val) => {
          return val.isActive === false
        })

        const filterActive = res.data.filter((val) => {
          return val.isActive === true
        })
        setNoOfActive(filterActive.length)
        setNoOfInactive(filterInactive.length)
      })
  }
  // state for search word
  const [wordEntered, setWordEntered] = useState('')

  let token = localStorage.getItem('token')

  const formatDate = (string) => {
    console.log(string)
    if (string === undefined) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  // function for search input
  const handleFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)
  }

  return (
    <div id="container" className="mt-3">
      <div className="title-holder d-flex">
        <h1 className="mx-auto mt-2">Behavior</h1>
      </div>
      <div
        className="mb-3"
        style={{
          marginLeft: '25px',
        }}
      >
        <Popup>
          <AddNewBehaviorForm />
        </Popup>
      </div>
      <div className="mb-5">
        <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography sx={{ width: '33%', flexShrink: 0 }}>
              Active Behavior ({noOfActive}){' '}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <BehaviorTable />
          </AccordionDetails>
        </Accordion>
        <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2bh-content"
            id="panel2bh-header"
          >
            <Typography sx={{ width: '33%', flexShrink: 0 }}>
              Inactive Behavior ({noOfInactive})
            </Typography>
          </AccordionSummary>
          <AccordionDetails></AccordionDetails>
        </Accordion>
        <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3bh-content"
            id="panel3bh-header"
          >
            <Typography sx={{ width: '33%', flexShrink: 0 }}>Episode Charting</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Nunc vitae orci ultricies, auctor nunc in, volutpat nisl. Integer sit amet egestas
              eros, vitae egestas augue. Duis vel est augue.
            </Typography>
          </AccordionDetails>
        </Accordion>
      </div>
    </div>
  )
}

export default Behavior
