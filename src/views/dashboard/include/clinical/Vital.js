import React, { useState, useEffect, useContext } from 'react'
import { Row, Col } from 'react-bootstrap'
import Popup from '../../popup/Popup'
import AddVitalForm from '../../form/AddVitalForm'
import EditIncidentReportForm from '../../form/EditIncidentReportForm'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import EditPopup from '../../popup/EditPopup'
import EditVitalForm from '../../form/EditVitalForm'
// MUI
import InputBase from '@material-ui/core/InputBase'
import { Grid, Box, CardActionArea } from '@mui/material'
// icons
import SearchIcon from '@mui/icons-material/Search'
import { DataGrid } from '@mui/x-data-grid'
// axios api
import api from '../../../../api/api'
// global variable
import AppContext from '../../../../AppContext'
import Swal from 'sweetalert2'

function Vital() {
  // state for search word
  const [wordEntered, setWordEntered] = useState('')
  const [isFetchDone, setIsFetchDone] = useState(true)

  let tenantId = localStorage.getItem('tenantId')

  const {
    vitalData,
    setVitalData,
    dailylogData,
    setDailylogData,
    incidentReportList,
    setIncidentReportList,
  } = useContext(AppContext)

  const fetchVitalData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/vitals/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setVitalData(res.data)
        setIsFetchDone(true)
      })
  }

  useEffect(() => {
    fetchVitalData()
  }, [tenantId])

  const deleteIncidentReportData = (e, id) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .put(`/vitals/${id}/delete`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)
            const newArr = [...vitalData]

            newArr.forEach((item, index) => {
              if (item.id === id) {
                newArr.splice(index, 1)
              }
            })

            setVitalData(newArr)
            Swal.fire('Deleted!', 'The Data has been deleted.', 'success')

            // adding history to database
            const input2 = {
              title: `Deleted ${tenantName}'s vital's report data`,
              tenantName: tenantName,
              tenantId: tenantId,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {})
              .catch((err) => {
                console.error(err)
              })
          })
          .catch((err) => {
            Swal.fire({
              title: 'fail to delete',
              icon: 'error',
            })
          })
      }
    })
  }

  const cutDate = (string) => {
    let date = new Date(string)
    // const month = date.toLocaleString('en-us', { month: 'long' }); /* June */

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    const hour = date.getHours() % 12 || 12
    const min = date.getMinutes()
    const sec = date.getSeconds()

    var ampm = date.getHours() >= 12 ? 'PM' : 'AM'
    const newDay = day < 10 ? '0' + day : day

    let fullDate =
      month + '/' + newDay + '/' + year + ' - ' + hour + ':' + min + ':' + sec + ' ' + ampm

    return fullDate
  }

  const columns = [
    {
      field: 'date',
      headerName: 'Date and Time',
      flex: 1,
      minWidth: 190,
      valueGetter: (params) => {
        return cutDate(params.row.date)
      },
    },
    {
      field: 'bodyTemp',
      headerName: 'Body Temperature',
      flex: 1,
      minWidth: 120,
    },

    {
      field: 'pulseRate',
      headerName: 'Pulse Rate',
      flex: 1,
      minWidth: 120,
    },
    {
      field: 'respirationRate',
      headerName: 'Respiration Rate',
      flex: 1,
      minWidth: 120,
    },
    {
      field: 'bloodPressure',
      headerName: 'Blood Pressure',
      flex: 1,
      minWidth: 120,
    },
    {
      field: 'createdBy',
      headerName: 'User',
      flex: 1,
      minWidth: 120,
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>
            <EditVitalForm data={params.row} />
          </EditPopup>
          <div
            id="delete-btn"
            className="btn d-flex"
            onClick={(e) => deleteIncidentReportData(e, params.row.id)}
          >
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  return (
    <div id="container" className="mt-3">
      <div className="d-flex">
        <h1 className="mx-auto mt-2">Vital</h1>
      </div>
      <Row>
        <Grid container rowSpacing={4} columnSpacing={{ xs: 1, sm: 1, md: 1 }}>
          <Grid
            item
            md={4}
            sm={10}
            direction="column"
            className="ml-5 pb-3"
            style={{
              marginLeft: '25px',
            }}
          >
            <Popup className="pb-5">
              <AddVitalForm />
            </Popup>
          </Grid>

          {/*<InputBase
            placeholder="Search…"
            value={wordEntered}
            onChange={handleFilter}
            inputProps={{ 'aria-label': 'search' }}
            endAdornment={<SearchIcon style={{ fontSize: 50 }} className="pr-3" />}
            id="searchBar2"
          />*/}
        </Grid>
        <Col md="12" className="mx-auto"></Col>
        <div id="datagrid-container" className="mx-auto">
          <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-3">
            {vitalData.length > 0 && isFetchDone == true ? (
              <DataGrid
                getRowId={(row) => row.id}
                rows={vitalData}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
              />
            ) : (
              <p>No Data Available</p>
            )}
          </Col>
        </div>
        <Col md="12" className="mx-auto"></Col>
      </Row>
    </div>
  )
}

export default Vital
