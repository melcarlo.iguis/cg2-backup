import React, { useEffect, useState, useContext } from 'react'
import api from '../../api/api'
import { Row, Col, Table } from 'react-bootstrap'
import TextField from '@mui/material/TextField'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import Autocomplete from '@mui/material/Autocomplete'
import EditPopup from './popup/EditPopup'
import Popup from './popup/Popup'
import { DataGrid } from '@mui/x-data-grid'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardHeader,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CCol,
  CRow,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import AddNewUser from './form/AddNewUser'
import AppContext from '../../AppContext'
import Products from './table/Products'

function Lookup() {
  const [isFetchDone, setIsFetchDone] = useState(false)
  const { userList, setUserList, allergiesList, setAllergiesList, currentTab, setCurrentTab } =
    useContext(AppContext)

  return (
    <CCard>
      <CCardHeader id="card-header">
        <strong id="res-title">Lookup</strong>
      </CCardHeader>
      <CCardBody>
        <CRow xs={{ gutterY: 3 }}>
          <div className="mt-3 mx-auto">
            <Col md="12" className="mx-auto mb-3 mt-5">
              <Products />
            </Col>
          </div>
        </CRow>
      </CCardBody>
    </CCard>
  )
}

export default Lookup
