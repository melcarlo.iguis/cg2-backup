import React, { useContext, useEffect, useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import Autocomplete from '@mui/material/Autocomplete'
import FormControlLabel from '@mui/material/FormControlLabel'
import SwitchUnstyled, { switchUnstyledClasses } from '@mui/base/SwitchUnstyled'
import SearchIcon from '@mui/icons-material/Search'
import TextField from '@mui/material/TextField'
import { styled } from '@mui/system'
import SosIcon from '@mui/icons-material/Sos'
// global variable
import AppContext from '../AppContext'
import api from '../api/api'
// import Form from 'react-bootstrap/Form'
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilSpeedometer,
  cilEnvelopeOpen,
  cilList,
  cilMenu,
  cilSearch,
  cilHome,
  cilHeart,
  cilNotes,
  cilPeople,
} from '@coreui/icons'

import { AppBreadcrumb } from './index'
import { AppHeaderDropdown } from './header/index'
import { logo } from 'src/assets/brand/logo'

const blue = {
  500: '#007FFF',
}

const grey = {
  400: '#BFC7CF',
  500: '#AAB4BE',
  600: '#6F7E8C',
}
const Root = styled('span')(
  ({ theme }) => `
  font-size: 0;
  position: relative;
  display: inline-block;
  width: 40px;
  height: 20px;
  margin: 10px;
  cursor: pointer;

  &.${switchUnstyledClasses.disabled} {
    opacity: 0.4;
    cursor: not-allowed;
  }

  & .${switchUnstyledClasses.track} {
    background: ${theme.palette.mode === 'dark' ? grey[600] : grey[400]};
    border-radius: 10px;
    display: block;
    height: 100%;
    width: 100%;
    position: absolute;
  }

  & .${switchUnstyledClasses.thumb} {
    display: block;
    width: 14px;
    height: 14px;
    top: 3px;
    left: 3px;
    border-radius: 16px;
    background-color: #fff;
    position: relative;
    transition: all 200ms ease;
  }

  &.${switchUnstyledClasses.focusVisible} .${switchUnstyledClasses.thumb} {
    background-color: ${grey[500]};
    box-shadow: 0 0 1px 8px rgba(0, 0, 0, 0.25);
  }

  &.${switchUnstyledClasses.checked} {
    .${switchUnstyledClasses.thumb} {
      left: 22px;
      top: 3px;
      background-color: #fff;
    }

    .${switchUnstyledClasses.track} {
      background: ${blue[500]};
    }
  }

  & .${switchUnstyledClasses.input} {
    cursor: inherit;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    opacity: 0;
    z-index: 1;
    margin: 0;
  }
  `,
)
const AppHeader = () => {
  let userType = localStorage.getItem('userType')
  const location = useLocation()
  const dispatch = useDispatch()
  const sidebarShow = useSelector((state) => state.sidebarShow)
  const {
    tenantList,
    setTenantList,
    actionValue,
    setActionValue,
    numberOfShiftLog,
    setNumberOfShiftLog,
    shiftLogData,
    setShiftLogData,
    isPrivacyOn,
    setIsPrivacyOn,
  } = useContext(AppContext)

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  const [wordEntered, setWordEntered] = useState('')

  const label = { componentsProps: { input: { 'aria-label': 'Demo switch' } } }
  // function for search input
  const handleSearchFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get('/residents/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        const newFilter = res.data.filter((value) => {
          let fullName = value.firstName + ' ' + value.middleName + ' ' + value.lastName

          return fullName.toLowerCase().includes(searchWord.toLowerCase())
        })
        setTenantList(newFilter)
      })
  }

  // function for search
  const handleFilter = (data) => {
    localStorage.setItem('tenantId', data)
  }

  return (
    <CHeader position="sticky" className="mb-4">
      <CContainer fluid>
        {/* <CHeaderToggler
          className="ps-1"
          onClick={() => dispatch({ type: 'set', sidebarShow: !sidebarShow })}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler> */}
        {/* <CHeaderBrand className="mx-auto d-md-none" to="/">
          <CIcon icon={logo} height={48} alt="Logo" />
        </CHeaderBrand> */}
        <CHeaderNav className="d-none d-md-flex me-auto">
          <CNavItem className={location.pathname == '/' ? 'header-item-active' : 'header-item'}>
            <CNavLink className="d-none d-md-flex" href="/">
              {/* <CIcon className="sidebar-brand-full" icon={cilHome} height={23} /> */}
              Home
            </CNavLink>
          </CNavItem>
          {userType != 'admin' ? (
            <>
              <CNavItem
                className={location.pathname == '/sos' ? 'header-item-active' : 'header-item'}
              >
                <CNavLink href="/sos">SOS</CNavLink>
              </CNavItem>
              <CNavItem>
                <div className="global-search">
                  <input type="text" placeholder="Search..."></input>
                  <div className="btn">
                    <SearchIcon />
                  </div>
                </div>
              </CNavItem>
              <CNavItem>
                {' '}
                <div id="switch-btn">
                  <FormControlLabel
                    label="Privacy"
                    value={isPrivacyOn}
                    onChange={(e) => setIsPrivacyOn(!isPrivacyOn)}
                    control={<SwitchUnstyled component={Root} {...label} />}
                  />
                </div>
              </CNavItem>
            </>
          ) : (
            <>
              <CNavItem
                className={location.pathname == '/admin' ? 'header-item-active' : 'header-item'}
              >
                <CNavLink href="/admin">Admin</CNavLink>
              </CNavItem>
              <CNavItem
                className={location.pathname == '/lookup' ? 'header-item-active' : 'header-item'}
              >
                <CNavLink href="/lookup">Lookup</CNavLink>
              </CNavItem>
              <CNavItem
                className={location.pathname == '/community' ? 'header-item-active' : 'header-item'}
              >
                <CNavLink href="/community">Community</CNavLink>
              </CNavItem>
            </>
          )}
        </CHeaderNav>
        <CHeaderNav>
          <CNavItem
            className={location.pathname == '/shiftlog' ? 'header-item-active' : 'header-item'}
          >
            {userType != 'admin' ? <CNavLink href="/shiftlog">Shift Summary Log</CNavLink> : null}
          </CNavItem>
        </CHeaderNav>
        <CHeaderNav className="ms-3">
          <AppHeaderDropdown id="drop-down-header" />
        </CHeaderNav>
      </CContainer>
      {/*<CHeaderDivider />*/}
      {/*<CContainer fluid><AppBreadcrumb /></CContainer>*/}
    </CHeader>
  )
}

export default AppHeader
